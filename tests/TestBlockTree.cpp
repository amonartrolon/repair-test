#include "gtest/gtest.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <includes/experimental/BlockTree1.h>
#include <includes/experimental/CBlockTree.h>
#include <includes/utils/InBufferIterator.h>
#include <includes/utils/StringUtils.h>
#include <ctime>

using namespace cds_utils;

string input_file = "/Users/aordonez/research/data/influenza.1MB.bin";
string input_file2 = "/Users/aordonez/research/data/influenza.bin";

TEST(TestBT1, BlockTreeLevel){
    uint64_t sigma = 15;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<uint32_t> raw_input = input;
    cds_static::BlockTreeLevel<uint32_t> bt1;
    bt1.build(input.cbegin(), input.cend(), input.size(), sigma, 16, &raw_input);
}

TEST(TestBT2, BlockTreeLevel2){
    uint64_t sigma = 15;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<uint32_t> raw_input = input;
    cds_static::BlockTreeLevel<uint32_t> bt1;
    bt1.build(input.cbegin(), input.cend(), input.size(), sigma, block_length, &raw_input);
}

TEST(TestBT2, BlockTreeLevelBool){
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }

    auto sigma = *std::max_element(input.cbegin(), input.cend());
    vector<bool> input_bool;
    for (auto x: input)
        input_bool.push_back(x>(sigma/2));
    vector<bool> raw_input = input_bool;

    cds_static::BlockTreeLevel<bool> bt1;
    bt1.build(input_bool.cbegin(), input_bool.cend(), input.size(), 2, block_length, &raw_input);
}


TEST(TestBT1, RecBlockTree1){
    uint64_t n = 1000000;
    uint64_t sigma = 15;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<uint32_t> raw_input = input;
    cds_static::BlockTree<uint32_t> bt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    bt1.build(input.cbegin(), input.cend(), n, sigma, std::move(strategy), &raw_input);
}


TEST(TestBT1, RecBlockTreeLZBounded){
    uint64_t n = 1000000;
    uint64_t sigma = 15;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<uint32_t> raw_input = input;
    cds_static::BTLZBounded<uint32_t> bt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    bt1.build(input.cbegin(), input.cend(), n, sigma, std::move(strategy), &raw_input);
}

TEST(TestBT1, RecBlockTreeByte){
    uint64_t n = 1000000;
    uint64_t sigma = 15;
    vector<byte> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<byte> raw_input = input;
    cds_static::BTLZBounded<byte> bt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    bt1.build(input.cbegin(), input.cend(), n, sigma, std::move(strategy), &raw_input);
}
/*TEST(TestBT2, BlockTreeBool){

    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    auto sigma = *std::max_element(input.cbegin(), input.cend()) + 1;
    vector<uint32_t> raw_input = input;

    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());

    cds_static::BTLZBounded<uint32_t> bt1;

    bt1.build(input.cbegin(), input.cend(), input.size(), sigma, std::move(strategy), &raw_input);
}*/

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
