#include "gtest/gtest.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <utils/RabinKarp.h>
#include <random>
#include <cstdint>
#include <utils/libcdsBasics.h>

using namespace cds_utils;

/*TEST(TestRabinKarp, Parameters){
    uint64_t d, m;
    d = 4;
    m = 14;
    RabinKarp<uint> rk(d, m);
    ASSERT_LE(bits(rk.q * rk.d), 64);
    ASSERT_EQ(rk.d, 3);
    ASSERT_GT(rk.h, 0);

    d = 870;
    m = 128;
    RabinKarp<uint> rk2(d, m);
    ASSERT_LE(bits(rk2.q * rk2.d), 64);
    ASSERT_EQ(rk2.d, 10);
    ASSERT_GT(rk2.h, 0);

    d = 4;
    m = 3;
    RabinKarp<uint> rk3(d, m);
    ASSERT_LE(bits(rk3.q * rk3.d), 64);
    ASSERT_EQ(rk3.d, 3);
    ASSERT_GT(rk3.h, 0);

    d = 15;
    m = 64;
    RabinKarp<uint> rk4(d, m);
    ASSERT_LE(bits(rk4.q * rk4.d), 64);
    ASSERT_EQ(rk4.d, 4);
    ASSERT_GT(rk4.h, 0);
}*/


TEST(TestRabinKarp, Roll){
  vector<uint> input = {2, 4, 2, 1, 4, 0, 0, 3, 2};
  uint m = 3;
  RabinKarp<uint> rk(4, m);

  auto s = rk.rkblock(&input[0], 0L, m);
  for (uint64_t x=1UL; x < input.size() - m -1; ++x){
    auto r = rk.rkblock(&input[0], x, x+m);
    s = rk.rkroll(s, input[x+m-1], input[x-1]);
    ASSERT_EQ(s, r);
  }
}

TEST(TestRabinKarp, RollWithVectors){
    vector<uint> input = {2, 4, 2, 1, 4, 0, 0, 3, 2};
    uint m = 3;
    RabinKarp<uint> rk(4, m);
    auto iter = input.cbegin();
    auto s = rk.rkblock(iter, m);
    iter = input.cbegin();
    for (uint64_t x=1UL; x < input.size() - m -1; ++x){
        auto i = iter+x;
        auto r = rk.rkblock(i, m);
        s = rk.rkroll(s, input[x+m-1], input[x-1]);
        ASSERT_EQ(s, r);
    }
}

TEST(TestRabinKarp, RollWithVectorsAndEnd){
    vector<uint> input = {2, 4, 2, 1, 4, 0, 0, 3, 2};
    uint m = 3;
    RabinKarp<uint> rk(4, m);
    auto iter = input.cbegin();
    auto s = rk.rkblock(iter, m);
    iter = input.cbegin();
    for (uint64_t x=1UL; x < input.size() -m -1; ++x){
        auto i = iter+x;
        auto r = rk.rkblock(i, input.cend(), m);
        s = rk.rkroll(s, input[x+m-1], input[x-1]);
        ASSERT_EQ(s, r);
    }
}

TEST(TestRabinKarp, RollLarge){
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uint b = 100;
    uint m = 64;
    std::uniform_int_distribution<uint> dis(0, b);

    vector<uint> input(1000);
    for (auto &x: input){
        x = dis(gen);
    }
    RabinKarp<uint> rk(b, m);
    auto s = rk.rkblock(&input[0], 0L, m);
    for (uint64_t x=1L; x < input.size() - m -1; ++x){
        auto r = rk.rkblock(&input[0], x, x+m);
        s = rk.rkroll(s, input[x+m-1], input[x-1]);
        ASSERT_EQ(s, r);
    }
}

TEST(TestRabinKarp, InputFile){
    string input_file = "/Users/aordonez/research/data/influenza.1MB.bin";
    cds_utils::InBufferIterator<uint32_t> input(1024, input_file, 0);
    uint64_t n = 1000000;
    uint64_t b = 15;
    uint64_t m = 64;
    RabinKarp<uint> rk(b, m);
    auto s = rk.rkblock(input, 0L, m);
    for (uint64_t x=1L; x <n - m -1; ++x){
        auto r = rk.rkblock(input, x, x+m);
        s = rk.rkroll(s, input[x+m-1], input[x-1]);
        ASSERT_EQ(s, r);
    }

}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
