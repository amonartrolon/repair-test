//
// Created by Alberto Ordóñez Pereira on 30/05/2017.
//

#include "gtest/gtest.h"
#include <includes/experimental/BlockTree1.h>
#include <includes/experimental/CBlockTree.h>
#include <includes/utils/StringUtils.h>
#include <algorithm>


using namespace cds_utils;

string input_file = "/Users/aordonez/research/data/influenza.1MB.bin";

class CBTFixture: public testing::Test{
protected:
    virtual void SetUp(){}
    virtual void TearDown(){}

    template<typename T> void TestAccess(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                    const cds_static::CBlockTree<T> &cbt1){
        uint64_t i = 0;
        for (; iter != end; ++iter, i++){
            auto a = *iter;
            auto b = cbt1[i];
            ASSERT_EQ(a, b) << "Position " << i;
        }
    }

    template<typename T>  void TestRank(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                    const cds_static::CBlockTree<T> &cbt1){
        vector<uint32_t> rank(cbt1.sigma()+1);
        uint64_t i = 0;
        for (; iter != end; ++iter, i++){
            auto a = ++rank[*iter];
            auto b = cbt1.rank(*iter, i);
            ASSERT_EQ(a, b) << "Position: " << i;
        }
    }

    template<typename T>  void TestSelect(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                                        const cds_static::CBlockTree<T> &cbt1){
        vector<uint32_t> select(cbt1.sigma()+1);
        uint64_t i = 0;
        for (; iter != end; ++iter, i++){
            ++select[*iter];
            auto b = cbt1.select(*iter, select[*iter]);
            ASSERT_EQ(i, b) << "Position: " << i;
        }
    }
};


TEST_F(CBTFixture, CBlockTree2Levels){
    uint64_t n = 1000000;
    uint64_t sigma = 15;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    vector<uint32_t> raw_input = input;

    cds_static::CBlockTree<uint32_t> cbt1;
    vector<uint64_t> block_lengths = {16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    cbt1.build(input.cbegin(), input.cend(), n, sigma, std::move(strategy), &raw_input);
    TestAccess<uint32_t>(input.cbegin(), input.cend(), cbt1);
    TestRank<uint32_t>(input.cbegin(), input.cend(), cbt1);
    TestSelect(input.cbegin(), input.cend(), cbt1);
    //std::cout << cbt1.stats() << std::endl;
    std::cout << cbt1.size_bytes() * 8.0 / cbt1.size() << std::endl;
}


TEST_F(CBTFixture, CBlockTree3Levels){
    uint64_t n = 1000000;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    uint64_t sigma = (*std::max_element(input.cbegin(), input.cend())) + 1;
    vector<uint32_t> raw_input = input;

    cds_static::CBlockTree<uint32_t> cbt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    cbt1.build(input.cbegin(), input.cend(), n, sigma, std::move(strategy));
    TestAccess<uint32_t>(input.cbegin(), input.cend(), cbt1);
    TestRank<uint32_t>(input.cbegin(), input.cend(), cbt1);
    TestSelect<uint32_t>(input.cbegin(), input.cend(), cbt1);
    std::cout << cbt1.size_bytes() * 8.0 / cbt1.size() << std::endl;

}

TEST_F(CBTFixture, CBlockTree3LevelsBytes){

    vector<byte> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }
    uint64_t sigma = static_cast<uint64_t>((*std::max_element(input.cbegin(), input.cend())) + 1);
    vector<byte> raw_input = input;

    cds_static::CBlockTree<byte> cbt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    cbt1.build(input.cbegin(), input.cend(), input.size(), sigma, std::move(strategy));
    TestAccess<byte>(input.cbegin(), input.cend(), cbt1);
    TestRank<byte>(input.cbegin(), input.cend(), cbt1);
    TestSelect<byte>(input.cbegin(), input.cend(), cbt1);
    std::cout << cbt1.size_bytes() * 8.0 / cbt1.size() << std::endl;

}

TEST_F(CBTFixture, CBlockTree3LevelsBool){
    uint64_t n = 1000000;
    vector<uint32_t> input;
    loadFile(input_file, input);
    uint64_t block_length = 16;
    for (uint64_t i=0; i < input.size() % block_length; i++){
        input.push_back(0);
    }

    auto sigma = *std::max_element(input.cbegin(), input.cend());
    vector<bool> input_bool;
    for (auto x: input)
        input_bool.push_back(x>(sigma/2));
    vector<bool> raw_input = input_bool;

    cds_static::CBlockTree<bool> cbt1;
    vector<uint64_t> block_lengths = {32, 16, 8};
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());
    cbt1.build(input_bool.cbegin(), input_bool.cend(), n, sigma, std::move(strategy));
    TestAccess<bool>(input_bool.cbegin(), input_bool.cend(), cbt1);
    TestRank<bool>(input_bool.cbegin(), input_bool.cend(), cbt1);
    TestSelect<bool>(input_bool.cbegin(), input_bool.cend(), cbt1);
    //std::cout << cbt1.stats() << std::endl;
    std::cout << cbt1.size_bytes() * 8.0 / cbt1.size() << std::endl;
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
