//
// Created by Alberto Ordóñez Pereira on 18/05/2017.
//

#include "gtest/gtest.h"
#include <includes/experimental/BlockTree1.h>

using namespace cds_utils;


TEST(TestBuffer, Add){

    cds_static::FixedSizeBuffer<int> fsb(3);
    fsb.push(1);
    ASSERT_EQ(fsb.front(), 1);
    fsb.push(2);
    ASSERT_EQ(fsb.front(), 1);
    fsb.push(3);
    ASSERT_EQ(fsb.front(), 1);
    fsb.push(3);
    ASSERT_EQ(fsb.front(), 3); //don't check bounds -> overwrites
}

TEST(TestBuffer, PopEmptyNotRaise) {

    cds_static::FixedSizeBuffer<int> fsb(3);
    fsb.front();
    fsb.pop_front();
}

TEST(TestBuffer, AddAndPop) {

    cds_static::FixedSizeBuffer<int> fsb(3);
    fsb.push(1);
    ASSERT_EQ(fsb.front(), 1);
    fsb.push(2);
    ASSERT_EQ(fsb.front(), 1);
    fsb.push(3);
    ASSERT_EQ(fsb.front(), 1);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 2);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 3);
}

TEST(TestBuffer, PopWithIterators) {

    vector<int> input = {1, 2, 3};
    cds_static::FixedSizeBuffer<int> fsb(input.cbegin(), input.cend());

    ASSERT_EQ(fsb.front(), 1);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 2);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 3);
}

TEST(TestBuffer, PopWithIteratorsN) {


    vector<int> input = {1, 2, 3};
    cds_static::FixedSizeBuffer<int> fsb(input.cbegin(), 3);

    ASSERT_EQ(fsb.front(), 1);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 2);
    fsb.pop_front();
    ASSERT_EQ(fsb.front(), 3);
}

TEST(TestBuffer, PushPopCircular) {
    vector<int> input = {1, 2, 3, 4, 5, 6, 7, 8};
    cds_static::FixedSizeBuffer<int> fsb(input.cbegin(), 3);
    uint32_t pfront = 0, pback = 3; 
    for (uint32_t i = 0 ; i < 5; i++){
        ASSERT_EQ(fsb.front(), input[pfront]);
        fsb.pop_front();
        fsb.push(input[pback++]);
        pfront++;
    }

}

    int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
