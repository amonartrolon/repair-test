#include <cassert>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sequence/Sequence.h>
#include <sequence/SequenceRepair.h>
#include <grammar/Repair.h>
#include <utils/libcdsBasics.h>

using namespace cds_static;
using namespace std;

void load_string_from_file(vector<uint>& data, char *filename) {
    std::ifstream f(filename, std::ios::binary);
    if(!f){
        cerr << "Problem in load string from file.\n" << endl;
    }
    f.seekg(0, std::ios::end);
    uint64_t size = f.tellg();
    f.seekg(0, std::ios::beg);
    unsigned char* str = new unsigned char[size + 1];
    f.read((char*) str, size);
    str[size] = 0;
    data.resize(size+1);
    for(int i=0;i<size+1;i++){
        data[i] =str[i];
    }
    MapperCont *mc = new MapperCont(data.data(),size+1,BitSequenceBuilderRG(32));
    for (size_t i=0;i<size+1;i++)
        data[i] = mc->map(data[i])-1;
    delete mc;

    delete[] str;
    f.close();
};

int main(int argc, char *argv[]) {

#ifdef MEM_MONITOR
    mm.event("GC-IS Init");
#endif

    if (argc != 7) {
        std::cerr << "Usage: ./repair_extract -c <file_to_be_encoded> <output>\n"
                  << "./repair_extract -e <encoded_file> <query file> <C sampling rate> <delta sampling rate> <ss_rate> <\n";
        exit(EXIT_FAILURE);
    }

    char* mode = argv[1];
    SequenceRepair* rp = NULL;
    int c_sampling_rate = atoi(argv[4]);
    int delta_sampling_rate = atoi(argv[5]);
    int ss_rate = atoi(argv[6]);
    
    if (strcmp(mode, "-c") == 0) {
        vector<uint> data;
        load_string_from_file(data,argv[2]);
        std::ofstream output(argv[3], std::ios::binary);
        cout << "Creating Repair Dictionary\n" << endl;
        BitSequenceBuilder **bmpsBuilders = new BitSequenceBuilder*[1];
        MapperCont *mc = new MapperCont(data.data(),data.size(),BitSequenceBuilderRG(32));
        bmpsBuilders[0] = new BitSequenceBuilderRPSC(new BitSequenceBuilderRRR(32),8,4,8u);
        // SequenceBuilder *wtRepairBuilder = new SequenceBuilderWaveletTreeNoptrs(bmpsBuilders[1],new MapperNone());
        // Sequence *wt = wtRepairBuilder->build(data.data(),data.size());
        rp = new SequenceRepairSC(data.data(),data.size(),bmpsBuilders[0],mc,nullptr,c_sampling_rate,delta_sampling_rate,ss_rate);
        cout << "Saving Repair Dictionary\n" << endl;
        rp->save(output);
        output.close();
    }
    else if(strcmp(mode,"-d")==0){
        cout << "Loading Repair Dictionary" << endl;
        std::ifstream input(argv[2], std::ios::binary);
        rp = SequenceRepairSC::load(input);
        cout << rp->access(1) << endl;
    }
    else if (strcmp(mode, "-e") == 0) {
        std::ifstream input(argv[2], std::ios::binary);
        std::ifstream query(argv[3]);
        rp = SequenceRepairSC::load(input);
        uint64_t l, r;
        auto first = std::chrono::high_resolution_clock::now();
        auto total_time = std::chrono::high_resolution_clock::now();
        while (query >> l) {
                auto t0 = std::chrono::high_resolution_clock::now();
                uint c = rp->access(l);
                auto t1 = std::chrono::high_resolution_clock::now();
                total_time += t1-t0;
                cout << c << endl;
        }
        std::chrono::duration<double> elapsed = total_time - first;
        cout << "Batch Extraction Total time(s): " << elapsed.count() << endl;

    } else {
        std::cerr << "Invalid mode: use -c for compression, -d for "
                     "decompression or -e for extraction.\n";
        exit(EXIT_FAILURE);
    }
    return 0;
}
