include_directories(${CMAKE_SOURCE_DIR}/includes)

add_executable(repair_extract_sc repair_extract_sc.cpp)
add_executable(repair_extract_sn repair_extract_sn.cpp)
target_link_libraries(repair_extract_sc LINK_PUBLIC cds pthread)
target_link_libraries(repair_extract_sn LINK_PUBLIC cds pthread)
install(TARGETS repair_extract_sc RUNTIME DESTINATION ${CMAKE_SOURCE_DIR}/bin)
install(TARGETS repair_extract_sn RUNTIME DESTINATION ${CMAKE_SOURCE_DIR}/bin)

