//
// Created by Alberto Ordóñez Pereira on 1/5/16.
//

#include <sequence/MappingWMCompact.h>

namespace  cds_static{

    MappingWMCompact::MappingWMCompact(uint sigma, uint minLevel, uint maxLevel, uint *symb2code, uint *symbSortByCode,
                                       uint *lastCode, uint *codes, uint *nCodesAtLevel):MappingWM(sigma,minLevel,maxLevel){

    }
    MappingWMCompact::MappingWMCompact(uint sigma, uint minLevel, uint maxLevel, tuple * table_symbol_code_length ):MappingWM(sigma,minLevel,maxLevel){

    }

    MappingWMCompact::~MappingWMCompact(){

    }

    uint MappingWMCompact::getSymbol(uint code, uint len){
        return 0;
    }

    void MappingWMCompact::getCode(uint symbol, uint &code, uint &len){

    }

    bool MappingWMCompact::finishCode(uint code, uint len){
        return true;
    }

    size_t MappingWMCompact::getSize(){
        return 0UL;
    }

    void MappingWMCompact::save(ofstream & fp){

    }

    MappingWMCompact * load(ifstream & fp){
        return nullptr;
    }

}
