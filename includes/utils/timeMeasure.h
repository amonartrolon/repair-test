#ifndef _INCLUDE_TIME_MEASURE_H
#define _INCLUDE_TIME_MEASURE_H

#include <sys/times.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>
#include <ctime>

namespace  cds_utils {
    double getTime(void) {

        double usertime;
        struct rusage usage;

        getrusage(RUSAGE_SELF, &usage);

        usertime = (double) usage.ru_utime.tv_sec + //(double)usage.ru_stime.tv_sec +
                (double) usage.ru_utime.tv_usec / 1000000.0;// + (double) usage.ru_stime.tv_usec / 1000000.0;

        return (usertime);
    }

    string currentDateTime () {
        std::chrono::time_point<std::chrono::system_clock> t;
        t = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(t);
        stringstream ss;
        ss << std::ctime(&end_time);
        return ss.str();
    };
}
#endif
