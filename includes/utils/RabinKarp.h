#ifndef RABIN_KARP_H_
#define RABIN_KARP_H_
#include "libcdsBasics.h"
#include <includes/utils/InBufferIterator.h>

namespace cds_utils{

template<typename T> class RabinKarp{
  /**
    Parameters. See CLRS for mor info but, summing up:
      b: number of different symbols in the sequence (sigma).
      q: modulo of the rk.
      h: b^(m-1).
      m: length of the blocks.
      Restrictions:
      We want this to be fast so we should avoid multiplications/modulo operations.
      The only way of doing that is to convert those operations in shift ops, which
      implies q and b to be multiples of 2 (as result, h will be also multiple of 2).
  */
  public:

    typedef uint64_t rktype;

    friend ostream& operator<<(ostream &os, const RabinKarp<T> &rk);
    RabinKarp (){}
    ~RabinKarp(){}

    RabinKarp(RabinKarp &&rk){
        this->d      = rk.d;
        this->q      = rk.q;
        this->bits_q = rk.bits_q;
        this->h      = rk.h;
        this->m      = rk.m;
    }

    RabinKarp& operator=(RabinKarp &&rk){
        if (this != &rk){
            this->d      = rk.d;
            this->q      = rk.q;
            this->bits_q = rk.bits_q;
            this->h      = rk.h;
            this->m      = rk.m;
        }
        return *this;
    }
    /**
      Constructor.
      _d: number of different elements (sigma)
      m: length of the Rabin-Karp window
    */
    RabinKarp(T _d, uint64_t _m){
        //First power of 2 greater or equal than b
        this->m = _m;
        this->d = bits64(_d); // The first power of 2 >= this-> _d
        assert(this->d < 64);
        this->q = get_prime(64-this->d-1); // The great prime number s.t. 2^(this->d) * this-> q < 2^64-1
        this->bits_q = bits64(q);
        //this->q = 1UL << this->bits_q;
        this->h = get_h(1<<this->d, m-1, this->q); // d^(m-1) % q
    }
    /**
    Computes the Rabin-Karp fingerprint for a given input
    */
    rktype rkblock(const T *input, uint64_t from, uint64_t to) const{
      assert(to>=from);
      assert((to-from)<=this->m);
      rktype r = static_cast<rktype>(0);
      for (auto x=from; x<to;x++){
        r = ((r<<this->d) + input[x]) % this->q;
      }
      return r;
    }

    /**
Computes the Rabin-Karp fingerprint for a given input
*/
    rktype rkblock(const vector<T> &input, uint64_t from, uint64_t to) const{
        assert(to>=from);
        assert((to-from)<=this->m);
        rktype r = static_cast<rktype>(0);
        for (auto x=from; x<to;x++){
            r = ((r<<this->d) + input[x]) % this->q;
        }
        return r;
    }

    /**
    Computes the Rabin-Karp fingerprint for a given input. The input is a const_iterator, and is
     modified during the operation (it moves forward block_length positions). It assumes it
     can move forward block_length positions, so this condition must be checked befor calling
     the function.
    */
    rktype rkblock(typename vector<T>::const_iterator &input,
                   const uint64_t block_length) const{

        assert(block_length<=this->m);
        rktype r = static_cast<rktype>(0);
        for (auto x=0UL; x<block_length;x++){
            r = ((r<<this->d) + (*input)) % this->q;
            ++input;
        }
        return r;
    }

    /**
    Computes the Rabin-Karp fingerprint for a given input. The input is a const_iterator, and is
    modified during the operation (it moves forward block_length positions). It assumes it
    can move forward block_length positions, so this condition must be checked befor calling
    the function.
    */
    rktype rkblock(typename vector<T>::const_iterator &input,
                   const typename vector<T>::const_iterator &end,
                   const uint64_t block_length) const{

        assert(block_length<=this->m);
        rktype r = static_cast<rktype>(0);
        for (uint64_t x=0; x < block_length && input != end;++x, ++input){
            r = ((r<<this->d) + (*input)) % this->q;
        }
        return r;
    }
    /**
    Computes the Rabin-Karp fingerprint for a given input
    */
    rktype rkblock(cds_utils::InBufferIterator<T> &input, uint64_t from, uint64_t to) const{
        assert(to>=from);
        assert((to-from)<=this->m);
        rktype r = static_cast<rktype>(0);
        for (auto x=from; x<to;x++){
            r = ((r<<this->d) + input[x]) % this->q;
        }
        return r;
    }
    /**
      Carries out a RK-roll.
      current: current rk value
      entering: element entering the windows
      leaving: element leaving the window
    */
    inline rktype rkroll(rktype current, T entering, T leaving) const{
        // if leaving * this->h < this-> q , leaving * this->h % this->q = leaving * this->h. But it isn't. That is
        // why the modulo appears. TODO: Come up with something better
        long long a = current - ((leaving * this->h) % this->q);
        //rktype a = (current - (leaving * this->h)) % this->q;
        if (a < 0){
            a += this->q;
        }
        return static_cast<rktype>((( a << this->d ) + entering) % this->q);
    }

  public:
    T d;
    uint64_t q;
    uint64_t bits_q;
    uint64_t h;
    uint64_t m;
private:
    //2^{8+i}-kprime[i] is the largest prime of length 8+i
    const uint64_t kprime[57] = {5,3,3,9,3,1,3,19,15,1,5,1,3,9,3,15,3,39,5,39,57,3,35,1,5,9,41,31,5,25,45,7,87,21,
                                   11,57,17,55,21,115,59,81,27,129,47,111,33,55,5,13,27,55,93,1,57,25,59};
    const uint64_t small_primes[8] = {3, 5, 11, 17, 37, 67, 131, 257};

    uint64_t get_prime(uint64_t nbits){
        return (1UL<<nbits)-kprime[nbits-8];
    }
    uint64_t get_h(uint64_t d, uint64_t m, uint64_t q){
        if (m == 1){
            return d % q;
        }else{
            return ((d % q) * get_h(d, m-1, q)) % q;
        }
    }

};

    template<typename T> ostream& operator<<(ostream &os, RabinKarp<T> &rk){
        os << "RabinKarp:\n\td: " << rk.d << "\n\tq: " << rk.q << "\n\th:" << rk.h << "\n\tm: " << rk.m;
        return os;
    }

};
#endif
