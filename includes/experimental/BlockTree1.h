//
// Created by Alberto Ordóñez Pereira on 11/05/2017.
//
#ifndef LIBCDS2_BlockTreeLevel_H
#define LIBCDS2_BlockTreeLevel_H

#include <algorithm>
#include <deque>
#include <fstream>
#include <unordered_map>
#include <includes/experimental/BlockTreeHelper.h>
#include <includes/experimental/CBlockTreeWrappers.h>
#include <includes/utils/InBufferIterator.h>
#include <includes/utils/RabinKarp.h>

using namespace std;

namespace cds_static {

template <typename T>
class BlockTreeLevel
{
public:
    typedef typename vector<bool>::const_iterator const_marks_iter;
    typedef typename vector<bool>::const_reverse_iterator rconst_marks_iter;
    typedef
        typename unordered_map<uint64_t, BlockContainer>::const_iterator const_hash_iter;
    typedef typename vector<T>::const_iterator input_iterator;
    typedef typename vector<T>::size_type size_type;

public:
    BlockTreeLevel()
    {
    }
    virtual ~BlockTreeLevel()
    {
    }

    BlockTreeLevel(
        input_iterator in, input_iterator end, uint64_t _n, uint64_t _sigma,
        uint64_t _block_length, vector<bool>&& _marks, vector<T>&& marked,
        unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer>&& h) :
        hash_(h),
        marks(_marks),
        marked_subsequence(marked),
        n(_n),
        sigma(_sigma),
        block_length(_block_length),
        input_iter(in),
        input_end_iter(end)
    {
    }

    BlockTreeLevel(BlockTreeLevel&& bt)
    {
        this->hash_ = std::move(bt.hash_);
        this->marks = std::move(bt.marks);
        this->marked_subsequence = std::move(bt.marked_subsequence);
        this->n = bt.n;
        this->sigma = bt.sigma;
        this->block_length = bt.block_length;
    }

    BlockTreeLevel& operator=(BlockTreeLevel&& bt)
    {
        if (this != &bt)
        {
            this->hash_ = std::move(bt.hash_);
            this->marks = std::move(bt.marks);
            this->marked_subsequence = std::move(bt.marked_subsequence);
            this->n = bt.n;
            this->sigma = bt.sigma;
            this->block_length = bt.block_length;
        }
        return *this;
    }
    virtual void build(input_iterator input, const input_iterator& end, const uint64_t n,
                       const uint64_t sigma, const uint64_t block_length,
                       const vector<T>* raw_input = nullptr);

    string stats() const;

    virtual const_hash_iter pointers_back_begin() const
    {
        return this->hash_.cbegin();
    };
    virtual const_marks_iter marks_begin() const
    {
        return this->marks.cbegin();
    }
    virtual rconst_marks_iter marks_rbegin() const
    {
        return this->marks.crbegin();
    }

    virtual input_iterator marked_subsequence_begin() const
    {
        return this->marked_subsequence.cbegin();
    }

    virtual const_hash_iter pointers_back_end() const
    {
        return this->hash_.cend();
    };
    virtual const_marks_iter marks_end() const
    {
        return this->marks.cend();
    }
    virtual rconst_marks_iter marks_rend() const
    {
        return this->marks.crend();
    }
    virtual input_iterator marked_subsequence_end() const
    {
        return this->marked_subsequence.cend();
    }

    size_type size_marked_subsequence()
    {
        return this->marked_subsequence.size();
    }
    size_type size_marks()
    {
        return this->marks.size();
    }
    size_type get_block_length() const
    {
        return this->block_length;
    }
    size_type size()
    {
        return this->n;
    }
    T get_sigma()
    {
        return this->sigma;
    }

    inline bool is_marked(typename vector<bool>::size_type i) const
    {
        return this->marks[i];
    }
    inline void set_mark(typename vector<bool>::size_type i, const bool m) const
    {
        this->marks[i] = m;
    }

    bool prepare_next_level(bool last_level, input_iterator& input, input_iterator& end,
                            uint64_t& length, BTLeaves<T>& leaves,
                            vector<T>*& r_input = nullptr);

protected:
    unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer> hash_;
    vector<bool> marks;
    vector<T> marked_subsequence;
    uint64_t n;
    uint64_t sigma;
    uint64_t block_length;
    input_iterator input_iter;
    input_iterator input_end_iter;
};

template <typename T>
void BlockTreeLevel<T>::build(input_iterator input, const input_iterator& end,
                              const uint64_t n, const uint64_t sigma,
                              const uint64_t block_length, const vector<T>* raw_input)
{
    cds_utils::RabinKarp<T> rk(sigma, block_length);
    this->input_iter = input;
    this->input_end_iter = end;
    this->n = n;
    this->sigma = sigma;
    this->block_length = block_length;
    FakeFingerprintContainer<> ffpc;
    this->hash_ = BlockTreeHelper::compute_block_hashes<T, input_iterator>(
        input, end, rk, this->n, this->block_length, this->block_length, ffpc, raw_input);
    this->marks = BlockTreeHelper::find_first_occurrences<T, input_iterator>(
        input, end, rk, this->hash_, this->n, this->block_length, this->block_length,
        raw_input);
    this->marked_subsequence = BlockTreeHelper::get_marked_blocks<T, input_iterator>(
        input, end, this->n, this->block_length, marks.cbegin(), marks.cend(), raw_input);
    assert(marks[0]);
}

template <typename T>
bool BlockTreeLevel<T>::prepare_next_level(bool last_level, input_iterator& input,
                                           input_iterator& end, uint64_t& length,
                                           BTLeaves<T>& leaves, vector<T>*& r_input)
{
    return BlockTreeHelper::prepare_next_level<T, input_iterator, BTLeaves<T>>(
        last_level, input, end, this->marked_subsequence_begin(),
        this->marked_subsequence_end(), length, leaves, r_input);
}

template <typename T>
string BlockTreeLevel<T>::stats() const
{
    stringstream ss;
    ss << "BlockTreeLevel stats: \n";
    ss << "\tInput size: " << this->n << endl;
    ss << "\tSigma: " << this->sigma << endl;
    ss << "\tBlock length: " << this->block_length << endl;
    int zeroes = count(marks.begin(), marks.end(), false);
    ss << "\tMarked Blocks: " << zeroes << endl;
    ss << "\tUnmarked Blocks: " << this->marks.size() - zeroes << endl;
    ss << "\tCompression: ~"
       << static_cast<double>(this->marked_subsequence.size()) / this->n * 100 << " %"
       << endl;
    return ss.str();
}

template <typename T>
class BlockTree
{
public:
    typedef typename vector<bool>::const_iterator const_marks_iter;
    typedef
        typename unordered_map<uint64_t, BlockContainer>::const_iterator const_hash_iter;
    typedef typename vector<T>::const_iterator input_iterator;
    typedef typename vector<T>::size_type size_type;

public:
    BlockTree()
    {
    }
    ~BlockTree()
    {
    }

    void build(input_iterator input, input_iterator end, const uint64_t n,
               const uint64_t sigma, StrategyBlockTree&& strategy,
               const vector<T>* raw_input = nullptr);

protected:
    uint64_t n_;
    uint64_t sigma_;
    vector<BlockTreeLevel<T>> levels_;
    StrategyBlockTree strategy_;
    std::vector<BlockTreeParams> params_;
    BTLeaves<T> leaves_;
};

template <typename T>
void BlockTree<T>::build(input_iterator input, input_iterator end, const uint64_t n,
                         const uint64_t sigma, StrategyBlockTree&& strategy,
                         const vector<T>* raw_input)
{
    this->strategy_ = strategy;
    this->sigma_ = sigma;
    this->n_ = n;

    uint64_t length = n;
    vector<T>* r_input = const_cast<vector<T>*>(raw_input);

    this->levels_ = vector<BlockTreeLevel<T>>(this->strategy_.levels());

    for (int32_t i = 0; i < this->strategy_.levels(); ++i)
    {
        this->params_.push_back(BlockTreeParams(length, this->strategy_.block_length(i)));
        this->levels_[i].build(input, end, length, sigma, this->strategy_.block_length(i),
                               r_input);
        this->levels_[i].prepare_next_level((i + 1 == this->strategy_.levels()), input,
                                            end, length, this->leaves_, r_input);
    }
}

template <typename T>
class BTLZBounded : public BlockTree<T>
{
public:
    typedef typename vector<bool>::const_iterator const_marks_iter;
    typedef
        typename unordered_map<uint64_t, BlockContainer>::const_iterator const_hash_iter;
    typedef typename vector<T>::const_iterator input_iterator;
    typedef typename vector<T>::size_type size_type;

public:
    BTLZBounded() : BlockTree<T>()
    {
    }
    ~BTLZBounded()
    {
    }

    void build(input_iterator input, input_iterator end, const uint64_t n,
               const uint64_t sigma, StrategyBlockTree&& strategy_,
               const vector<T>* raw_input = nullptr);
    void replace_blocks(vector<BTLZBoundedLevelHelper<T, input_iterator>>& level);
    bool replace(vector<BTLZBoundedLevelHelper<T, input_iterator>>& levels,
                 uint32_t level, uint64_t index);
};

/**
 * In a first pass we find the first occurrences of blocks of length block_length. After
 * that, we compute which blocks are marked if we use blocks of block_length' = 2 *
 * block_length with an overlap of block_length. By doing so, merging the first list of
 * marked blocks and the second, we obtain the list of blocks that remain marked to be
 * passed to the next level.
 *
 * We carry out this process recursively and when the tree is built, we carry out a
 * pruning process that consists of removing all those blocks that are safely-removable.
 * The function replace() tells which is a safely-removable block.
 * @tparam T
 * @param input
 * @param end
 * @param n
 * @param sigma
 * @param strategy
 * @param raw_input
 */
template <typename T>
void BTLZBounded<T>::build(input_iterator input, input_iterator end, const uint64_t n,
                           const uint64_t sigma, StrategyBlockTree&& strategy,
                           const vector<T>* raw_input)
{
    this->strategy_ = strategy;
    this->sigma_ = sigma;
    this->n_ = n;

    uint64_t length = n;
    vector<T>* r_input = const_cast<vector<T>*>(raw_input);
    vector<vector<bool>> used_marks;

    vector<BTLZBoundedLevelHelper<T, input_iterator>> tmp_levels(
        this->strategy_.levels());

    for (int32_t i = 0; i < this->strategy_.levels(); ++i)
    {
        [&] {
            this->params_.push_back(
                BlockTreeParams(length, this->strategy_.block_length(i)));

            auto block_length = this->strategy_.block_length(i);
            tmp_levels[i].input = input;
            tmp_levels[i].end = end;
            tmp_levels[i].rk =
                cds_utils::RabinKarp<T>(sigma, this->strategy_.block_length(i));
            tmp_levels[i].hash = BlockTreeHelper::compute_block_hashes<
                T, input_iterator,
                VectorFingerprintContainer<typename cds_utils::RabinKarp<T>::rktype>>(
                input, end, tmp_levels[i].rk, length, block_length, block_length,
                tmp_levels[i].fingerprints, raw_input);
            tmp_levels[i].used_marks =
                BlockTreeHelper::find_first_occurrences<T, input_iterator>(
                    input, end, tmp_levels[i].rk, tmp_levels[i].hash, length,
                    block_length, block_length, raw_input);
            tmp_levels[i].marks = tmp_levels[i].used_marks;

            uint64_t block_length_2 = 2 * this->strategy_.block_length(i);
            cds_utils::RabinKarp<T> rk2(sigma, block_length_2);
            FakeFingerprintContainer<> ffpc;
            auto two_blocks_hash =
                BlockTreeHelper::compute_block_hashes<T, input_iterator,
                                                      FakeFingerprintContainer<>>(
                    input, end, rk2, length, block_length_2, block_length, ffpc,
                    raw_input);

            auto marks2 = BlockTreeHelper::find_first_occurrences(
                input, end, rk2, two_blocks_hash, length, block_length_2, block_length,
                raw_input);

            auto zeroes_0 = std::count(tmp_levels[i].used_marks.cbegin(),
                                       tmp_levels[i].used_marks.cend(), true);
            [&] {
                for (auto iter = marks2.cbegin(); iter != marks2.cend(); ++iter)
                {
                    if (*iter)
                    {
                        auto block = iter - marks2.cbegin();
                        tmp_levels[i].marks[block] = true;
                        tmp_levels[i].marks[block + 1] = true;
                    }
                }
            }();

            auto zeroes_1 = std::count(tmp_levels[i].marks.cbegin(),
                                       tmp_levels[i].marks.cend(), true);

            cout << "Zeroes 0: " << zeroes_0 << ", zeroes_1: " << zeroes_1
                 << ", total: " << tmp_levels[i].marks.size() << endl;

            tmp_levels[i].sub_sequence =
                BlockTreeHelper::get_marked_blocks<T, input_iterator>(
                    input, end, n, block_length, tmp_levels[i].marks.cbegin(),
                    tmp_levels[i].marks.cend());
            BlockTreeHelper::prepare_next_level(
                (i + 1 == this->strategy_.levels()), input, end,
                tmp_levels[i].sub_sequence.cbegin(), tmp_levels[i].sub_sequence.cend(),
                length, this->leaves_, r_input);
        }();
    }
    replace_blocks(tmp_levels);
}

/**
 * Triggers a DFS (from top to bottom and from right to left) traversal of the block tree,
 * calling the function replace to remove all blocks which are safely-removable.
 * @tparam T
 * @param levels
 */
template <typename T>
void BTLZBounded<T>::replace_blocks(
    vector<BTLZBoundedLevelHelper<T, typename BTLZBounded<T>::input_iterator>>& levels)
{
    uint64_t index = levels[0].marks.size() - 1;

    for (auto iter_marks = levels[0].marks.crbegin(),
              iter_used_marks = levels[0].used_marks.crbegin();
         iter_marks != levels[0].marks.crend(); ++iter_marks, ++iter_used_marks)
    {
        if (!*iter_used_marks && *iter_marks)
        {
            replace(levels, 0, index);
        }
        if (*iter_marks)
        {
            --index;
        }
    }
}
/**
 * Recursively checks if a block is safely removable. If so, it is marked with a 0 and we
 * store the pointers back to its first occurrence.
 * @tparam T
 * @param bt A block tree
 * @param level level of the recursion tree
 * @param index index of block in the bt at level "level"
 * @return True if a block can be safely-removable, false otherwise.
 */
template <typename T>
bool BTLZBounded<T>::replace(
    vector<BTLZBoundedLevelHelper<T, typename BTLZBounded<T>::input_iterator>>& bt,
    uint32_t level, uint64_t index)
{
    if (level == this->strategy_.levels() - 1)
    {
        cout << "Last level" << endl;
        return !bt[level].used_marks[index]; // if the blocks is flagged cannot replace
    }
    if (!bt[level].marks[index])
    {
        // cout << "Not marked block -> can replace" << endl;
        return true; // if the block is not marked --> we can replace it
    }
    if (bt[level].used_marks[index])
    {
        cout << "Primarly block" << endl;
        return false; // it is a primarly block which cannot be replace (somebody points
                      // to it)
    }

    cout << "Rest" << endl;
    assert(!bt[level].used_marks[index] && bt[level].marks[index]);

    auto right = replace(bt, level + 1, index * 2 + 1);
    auto left = replace(bt, level + 1, index * 2);

    if (right and left)
    {
        auto v = bt[level].hash.find(bt[level].fingerprints[index]);

        auto pos = index * this->strategy_.block_length(level);
        for (auto iter = v->second.begin(); iter != v->second.end();)
        {
            if (iter->block_pos == pos)
            {
                bt[level]
                    .used_marks[iter->first_occ / this->strategy_.block_length(level)] =
                    true;
                bt[level]
                    .used_marks[(iter->first_occ + this->strategy_.block_length(level)) /
                                this->strategy_.block_length(level)] = true;
            }
        }
        cout << "Raplacing " << endl;
        // replace the current index
        return true;
    }
    return false;
}
};
#endif // LIBCDS2_BlockTreeLevel_H
