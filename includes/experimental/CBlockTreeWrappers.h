//
// Created by Alberto Ordóñez Pereira on 29/05/2017.
//

#ifndef LIBCDS2_CBLOCKTREEWRAPPERS_H
#define LIBCDS2_CBLOCKTREEWRAPPERS_H
#include <bitsequence/BitSequenceBuilder.h>
#include <direct_access/DirectAccess.h>

namespace cds_static {

struct BlockTreeParams
{
    BlockTreeParams(uint64_t _n, uint64_t _bl, uint64_t _degree = 0) :
        n(_n),
        block_length(_bl),
        degree(_degree)
    {
    }
    uint64_t n;
    uint64_t block_length;
    uint64_t degree;
    uint64_t size_bytes() const
    {
        return sizeof(this);
    }
};

class StrategyBlockTree
{
public:
    StrategyBlockTree()
    {
    }
    StrategyBlockTree(std::vector<uint64_t>::const_iterator block_lengths_iter,
                      const std::vector<uint64_t>::const_iterator end)
    {
        this->block_lengths.resize(0);
        for (; block_lengths_iter != end; ++block_lengths_iter)
        {
            block_lengths.push_back(*block_lengths_iter);
        }
    }
    StrategyBlockTree(StrategyBlockTree& st)
    {
        this->block_lengths.resize(0);
        for (vector<uint64_t>::size_type i = 0; i < st.levels(); i++)
        {
            block_lengths.push_back(st.block_length(i));
        }
    }
    ~StrategyBlockTree()
    {
    }

    inline uint64_t levels() const
    {
        return this->block_lengths.size();
    }
    inline uint64_t block_length(uint32_t level) const
    {
        if (level < this->block_lengths.size())
        {
            return this->block_lengths[level];
        }
        return 0;
    }

protected:
    vector<uint64_t> block_lengths;
};

class CompressedPointersBack
{
public:
    CompressedPointersBack() : indices_(nullptr), offsets_(nullptr)
    {
    }

    CompressedPointersBack(CompressedPointersBack&& cb) :
        indices_(cb.indices_),
        offsets_(cb.offsets_)
    {
        cb.indices_ = nullptr;
        cb.offsets_ = nullptr;
    }
    CompressedPointersBack& operator=(CompressedPointersBack&& cb)
    {
        if (this != &cb)
        {
            this->indices_ = cb.indices_;
            this->offsets_ = cb.offsets_;
            cb.indices_ = nullptr;
            cb.offsets_ = nullptr;
        }
        return *this;
    }

    CompressedPointersBack(vector<uint32_t>& _off, vector<uint32_t>& _ind)
    {
        this->indices_ = new cds_static::DAC(&_ind[0], _ind.size(), false);
        this->offsets_ = new cds_static::DAC(&_off[0], _off.size(), false);
    }
    CompressedPointersBack(vector<uint64_t>::const_iterator iter_off,
                           const vector<uint64_t>::const_iterator end_off,
                           vector<uint64_t>::const_iterator iter_ind,
                           const vector<uint64_t>::const_iterator end_ind)
    {
        vector<uint32_t> _ind;
        _ind.insert(_ind.begin(), iter_ind, end_ind);
        this->indices_ = new cds_static::DAC(&_ind[0], _ind.size(), false);
        vector<uint32_t> _off;
        _off.insert(_off.begin(), iter_off, end_off);
        this->offsets_ = new cds_static::DAC(&_off[0], _off.size(), false);
    }

    ~CompressedPointersBack()
    {
        if (this->indices_)
            delete this->indices_;
        if (this->offsets_)
            delete this->offsets_;
    }

    uint64_t indice(const uint64_t pos) const
    {
        return this->indices_->access(pos);
    }

    uint64_t offset(const uint64_t pos) const
    {
        return this->offsets_->access(pos);
    }

    uint64_t size_bytes() const
    {
        return sizeof(this) + this->offsets_->getSize() + this->indices_->getSize();
    }

protected:
    cds_static::DirectAccess* indices_ = nullptr;
    cds_static::DirectAccess* offsets_ = nullptr;
};

template <typename T = uint32_t>
struct CompressedRankSamples
{
    CompressedRankSamples(std::vector<std::vector<T>>& _ranks)
    {
        for (auto& v : _ranks)
        {
            ranks.push_back(new DAC(&v[0], v.size()));
        }
    }
    ~CompressedRankSamples()
    {
        for (auto x : ranks)
            if (x)
                delete x;
    }

    CompressedRankSamples(CompressedRankSamples&& m) : ranks(std::move(m.ranks))
    {
        m.ranks.resize(0);
    }

    CompressedRankSamples&& operator=(CompressedRankSamples&& m)
    {
        if (this != &m)
        {
            this->ranks = std::move(m.ranks);
            m.ranks.resize(0);
        }
        return *this;
    }
    const cds_static::DirectAccess& operator[](size_t pos) const
    {
        return *(ranks[pos]);
    }
    uint64_t size_bytes() const
    {
        uint64_t total = 0;
        for (auto r : ranks)
        {
            if (r)
            {
                total += r->getSize();
            }
        }
        // total+=sizeof(this);
        return total;
    }
    vector<cds_static::DirectAccess*> ranks;
};

class CompressedMarks
{
public:
    CompressedMarks(std::vector<bool>::const_iterator iter,
                    const std::vector<bool>::const_iterator end)
    {
        uint32_t* bmp = nullptr;
        auto len = end - iter;
        cds_utils::createEmptyBitmap(&bmp, len);
        uint64_t i = 0;
        for (; iter != end; ++iter, ++i)
        {
            if (*iter)
            {
                cds_utils::bit_set(bmp, i);
            }
        }
        BitSequenceBuilderRRR bsb(33);
        this->bs = bsb.build(bmp, len);
        assert(this->bs);
        delete[] bmp;
    }

    CompressedMarks(CompressedMarks&& m) : bs(m.bs)
    {
        m.bs = nullptr;
    }
    CompressedMarks& operator=(CompressedMarks&& m)
    {
        if (this != &m)
        {
            this->bs = m.bs;
            m.bs = nullptr;
        }
        return *this;
    }

    ~CompressedMarks()
    {
        if (this->bs)
            delete this->bs;
    }

    inline bool operator[](size_t pos) const
    {
        return bs->access(pos);
    }
    uint64_t rank0(uint64_t pos) const
    {
        return bs->rank0(pos);
    }
    uint64_t rank1(uint64_t pos) const
    {
        return bs->rank1(pos);
    }
    uint64_t select0(uint64_t pos) const
    {
        return bs->select0(pos);
    }
    uint64_t select1(uint64_t pos) const
    {
        return bs->select1(pos);
    }
    size_t size() const
    {
        return bs->getLength();
    }
    size_t size_bytes() const
    {
        if (bs)
            return bs->getSize();
        return 0UL;
    }

private:
    cds_static::BitSequence* bs = nullptr;
};

template <typename T>
struct BTLeaves
{
    BTLeaves()
    {
    }
    BTLeaves(typename vector<T>::const_iterator iter,
             const typename vector<T>::const_iterator end)
    {
        this->leaves.resize(0);
        this->leaves.insert(this->leaves.begin(), iter, end);
    }
    BTLeaves(vector<T>&& _leaves) : leaves(_leaves)
    {
    }

    BTLeaves(BTLeaves&& c) : leaves(std::move(c.leaves))
    {
    }

    BTLeaves& operator=(BTLeaves&& c)
    {
        if (this != &c)
        {
            this->leaves = std::move(c.leaves);
        }
        return *this;
    }

    const T& operator[](const typename vector<T>::size_type i) const
    {
        return const_cast<T&>(this->leaves[i]);
    }

    typename vector<T>::size_type rank(const T& symbol,
                                       const typename vector<T>::size_type to,
                                       const typename vector<T>::size_type from = 0) const
    {
        typename vector<T>::size_type rnk = 0;
        for (auto i = from; i <= to; ++i)
        {
            if (leaves[i] == symbol)
                rnk++;
        }
        return rnk;
    }
    typename vector<T>::size_type select(const T& symbol, typename vector<T>::size_type s,
                                         typename vector<T>::size_type to = 0,
                                         typename vector<T>::size_type from = 0) const
    {
        auto i = from;
        for (; i < to && s != 0; ++i)
        {
            if (leaves[i] == symbol)
            {
                --s;
            }
        }
        return i - from - 1;
    }
    uint64_t size_bytes() const
    {
        return sizeof(T) * leaves.size();
    }
    vector<T> leaves;
};

template <>
const bool& BTLeaves<bool>::operator[](const typename vector<bool>::size_type i) const
{
    return static_cast<const bool&>(this->leaves[i]);
}
};
#endif // LIBCDS2_CBLOCKTREEWRAPPERS_H
