//
// Created by Alberto Ordóñez Pereira on 15/05/2017.
//

#ifndef LIBCDS2_BLOCKTREEHELPER_H
#define LIBCDS2_BLOCKTREEHELPER_H
#include <vector>
#include <includes/utils/InBufferIterator.h>

using namespace std;

namespace cds_static {

constexpr uint64_t NOT_SET = -1UL;

struct Block
{
    Block(uint64_t _id, uint64_t _bp) : id(_id), block_pos(_bp), first_occ(NOT_SET)
    {
    }
    uint64_t id;
    uint64_t block_pos;
    uint64_t first_occ;
    friend ostream& operator<<(ostream& os, const Block& b);
};

struct CompareBlocksIteratorsFirstOcc
{
    bool operator()(const vector<Block>::const_iterator& a,
                    const vector<Block>::const_iterator& b) const
    {
        return (*a).first_occ < (*b).first_occ;
    }
};

ostream& operator<<(ostream& os, const Block& b)
{
    os << b.id << ", " << b.first_occ << ", " << b.block_pos;
    return os;
}

class BlockContainer
{
public:
    BlockContainer(uint64_t _id, uint64_t _block_pos) : id(_id), block_pos(_block_pos)
    {
        blocks.push_back(Block(this->id, this->block_pos));
    }
    ~BlockContainer()
    {
    }
    void add_block(uint64_t _id, uint64_t _block_pos)
    {
        this->blocks.push_back(Block(_id, _block_pos));
    }
    vector<Block>::iterator begin()
    {
        return blocks.begin();
    }
    vector<Block>::iterator end()
    {
        return blocks.end();
    }
    vector<Block>::const_iterator cbegin() const
    {
        return blocks.cbegin();
    }
    vector<Block>::const_iterator cend() const
    {
        return blocks.cend();
    }

    vector<Block>::iterator& set_as_marked(vector<Block>::iterator& it)
    {
        ++it;
        return it;
    }
    /*iterator &set_as_marked(vector<Block>::iterator &it){
        this->blocks_set.push_back(*it);
        if (this->blocks.size() > 1) {
            *it = std::move(*(this->blocks.end() - 1));
        }
        this->blocks.erase(this->blocks.end()-1);
        return it;
    }*/
protected:
    uint64_t id;
    uint64_t block_pos;
    vector<Block> blocks;
    // vector<Block> blocks_set;
};

template <typename T>
class FixedSizeBuffer
{
public:
    typedef typename vector<T>::size_type size_type;

    FixedSizeBuffer()
    {
    }
    ~FixedSizeBuffer()
    {
    }
    FixedSizeBuffer(typename vector<T>::size_type _size) :
        size(_size),
        pfront(0),
        ptail(_size - 1)
    {
        this->buffer = vector<T>(size);
    }
    FixedSizeBuffer(typename vector<T>::const_iterator it,
                    const typename vector<T>::const_iterator& end)
    {
        this->ptail = 0;
        for (; it != end; ++it)
        {
            this->buffer.push_back(*it);
            this->ptail++;
        }
        this->size = this->ptail;
        this->ptail--;
        this->pfront = 0;
    }

    FixedSizeBuffer(typename vector<T>::const_iterator it, size_type n)
    {
        this->ptail = 0;
        for (size_type i = 0; i < n; ++i, ++it)
        {
            this->buffer.push_back(*it);
            this->ptail++;
        }
        this->size = this->ptail;
        this->ptail--;
        this->pfront = 0;
    }

    void push(T value)
    {
        increment_counter(this->ptail);
        buffer[this->ptail] = value;
    }
    T front()
    {
        return this->buffer[this->pfront];
    }
    void pop_front()
    {
        increment_counter(this->pfront);
    }

protected:
    vector<T> buffer;
    typename vector<T>::size_type size;
    typename vector<T>::size_type pfront, ptail;

    void increment_counter(typename vector<T>::size_type& pointer)
    {
        pointer++;
        if (pointer == this->size)
        {
            pointer = 0;
        }
    }
};

template <typename T = uint64_t>
struct FakeFingerprintContainer
{
    inline void push(T& x){};
    inline const T& operator[](const uint64_t i) const
    {
        return 0;
    }
};

template <typename T = uint64_t>
struct VectorFingerprintContainer
{
    inline void push(T& x)
    {
        this->fp.push_back(x);
    }
    inline const T& operator[](const uint64_t i) const
    {
        return fp[i];
    }
    vector<T> fp;
};

template <typename T, typename input_iterator>
struct BTLZBoundedLevelHelper
{
    input_iterator input;
    input_iterator end;
    vector<bool> used_marks;
    vector<bool> marks;
    unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer> hash;
    vector<T> sub_sequence;
    cds_utils::RabinKarp<T> rk;
    VectorFingerprintContainer<typename cds_utils::RabinKarp<T>::rktype> fingerprints;
};

class BlockTreeHelper
{
public:
    template <typename T>
    static bool check_pointers_back_integrity(
        const unordered_map<uint64_t, BlockContainer> hash_, const vector<bool>& marks,
        const uint64_t block_length, const uint64_t n, const vector<T>& raw_input)
    {
        for (const auto iter : hash_)
        {
            for (auto it = iter.second.cbegin(); it != iter.second.cend(); ++it)
            {
                assert(it->first_occ != NOT_SET);
                assert(marks[it->first_occ / block_length]);
                if (it->first_occ % block_length)
                {
                    assert(marks[it->first_occ / block_length + 1]);
                }
                uint64_t b_length = std::min(n - block_length, block_length);
                for (uint64_t i = 0; i < b_length; i++)
                {
                    assert(raw_input[it->first_occ + i] == raw_input[it->block_pos + i]);
                }
            }
        }
        return true;
    }

    template <typename T>
    static vector<T> compute_rank_zeroes(vector<bool>::const_iterator input,
                                         const vector<bool>::const_iterator end)
    {
        vector<T> ranks;
        T rank0 = 0;
        for (; input != end; ++input)
        {
            if (!(*input))
            {
                rank0++;
            }
            ranks.push_back(rank0);
        }
        return ranks;
    }

    template <typename T>
    static bool check_equality_iter_vector(typename vector<T>::const_iterator iter,
                                           const typename vector<T>::const_iterator end,
                                           const vector<T>& reference)
    {
        typename vector<T>::size_type i = 0;
        for (; iter != end; ++iter, ++i)
        {
            assert(*iter == reference[i]);
        }
    }

    template <typename T>
    static bool compare_segments(const vector<T>& input, uint64_t start1, uint64_t start2,
                                 uint64_t length)
    {
        for (uint64_t i = 0; i < length; i++)
        {
            if (input[start1 + i] != input[start2 + i])
            {
                return false;
            }
        }
        return true;
    }
    /**
     *  Stores in res[1..sigma][..] the rank offset between rank_blocks and the counter.
     * @tparam T
     * @param counters
     * @param rank_blocks
     * @param res
     * @param index_res position in res to store the values
     */
    template <typename T, typename T2>
    static void clone_counters(typename vector<T>::const_iterator input,
                               const typename vector<T>::const_iterator end,
                               vector<T2>& counters, vector<vector<T2>>& rank_blocks,
                               vector<vector<T2>>& rank_offsets,
                               vector<vector<T2>>& rank_offsets_mid,
                               typename vector<vector<T2>>::size_type index_res)
    {
        uint64_t i = 0;
        for (auto& it : counters)
        {
            rank_offsets[i][index_res] = it - rank_blocks[i].back();
            ++i;
        }
        // compute the rank until the block boundary
        for (; input != end; ++input)
        {
            auto x = static_cast<uint32_t>(*input);
            rank_offsets_mid[x][index_res]++;
        }
    }

    template <typename T, typename input_iterator,
              typename FingerPrintContainer = FakeFingerprintContainer<>>
    static unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer>
    compute_block_hashes(input_iterator input, const input_iterator& end,
                         const cds_utils::RabinKarp<T>& rk, const uint64_t n,
                         const uint64_t block_length, uint64_t shift,
                         FingerPrintContainer& fpc, const vector<T>* raw_input = nullptr)
    {
        unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer> hash;
        auto not_found = hash.end();
        auto shift_back = block_length - shift;
        auto limit = n - block_length;
        for (uint64_t i = 0; i <= limit; i += shift)
        {
            auto fingerprint = rk.rkblock(input, end, std::min(block_length, n - i));
            fpc.push(fingerprint);
            auto v = hash.find(fingerprint);
            input -= shift_back; // only necessary if we permit block overlapping
            if (v == not_found)
            {
                hash.insert(std::pair<uint64_t, BlockContainer>(
                    fingerprint, BlockContainer(fingerprint, i)));
            }
            else
            {
                v->second.add_block(fingerprint, i);
            }
        }
        return hash;
    }

    /**
     * Finds the first occurrence of each block and mark all blocks that are part of the
     * first occurrence of any block. Returns the vector or marks and updates the first
     * occurrence of each block in the hash.
     * @tparam T
     * @tparam input_iterator
     * @param input
     * @param end
     * @param n
     * @param block_length
     * @param hash
     * @param rk
     * @param raw_input
     * @return
     */
    template <typename T, typename input_iterator>
    static vector<bool> find_first_occurrences(
        input_iterator input, const input_iterator& end,
        const cds_utils::RabinKarp<T>& rk,
        unordered_map<typename cds_utils::RabinKarp<T>::rktype, BlockContainer>& hash,
        const uint64_t n, const uint64_t block_length, uint64_t shift,
        const vector<T>* raw_input)
    {
        vector<bool> marks =
            vector<bool>(std::ceil((n - (block_length - shift)) / shift));
        auto not_found = hash.end();
        auto stop = n - block_length;

        FixedSizeBuffer<T> window(input, block_length);

        auto fingerprint = rk.rkblock(input, end, block_length);

        marks[0] = true;
        for (uint64_t i = 0; i < stop; ++input)
        {
            auto first_occ = i + 1;
            fingerprint = rk.rkroll(fingerprint, *input, window.front());
            window.pop_front();
            window.push(*input);
            assert(!raw_input || rk.rkblock(*raw_input, first_occ,
                                            first_occ + block_length) == fingerprint);

            auto v = hash.find(fingerprint);
            if (v != not_found)
            {
                for (auto iter = v->second.begin(); iter != v->second.end();)
                {
                    if (iter->first_occ != NOT_SET)
                    {
                        // we have already set all the pointers (since we don't check if
                        // blocks are equal, all with the same fingerprint are pointed to
                        // the same block --> If some is marked, all should be marked as
                        // well
                        break;
                    }
                    /*if (raw_input && !BlockTreeHelper::compare_segments(*raw_input,
                    first_occ, iter->block_pos, std::min(block_length,
                    n-iter->block_pos))){ cout << iter->block_pos << endl;
                    }*/

                    assert(!raw_input ||
                           BlockTreeHelper::compare_segments(
                               *raw_input, first_occ, iter->block_pos,
                               std::min(block_length, n - iter->block_pos)));
                    assert(first_occ < n);
                    iter->first_occ = first_occ;
                    marks[first_occ / block_length] = true;
                    marks[(i + block_length) / block_length] = true;
                    iter = v->second.set_as_marked(iter);
                }
            }
            i = first_occ;
        }
        assert(!raw_input || BlockTreeHelper::check_pointers_back_integrity(
                                 hash, marks, block_length, n, *raw_input));
        return marks;
    }

    template <typename T, typename input_iterator>
    static vector<T> get_marked_blocks(input_iterator input, const input_iterator& end,
                                       const uint64_t n, const uint64_t block_length,
                                       vector<bool>::const_iterator iter,
                                       vector<bool>::const_iterator iter_end_marks,
                                       const vector<T>* raw_input = nullptr)
    {
        vector<T> res;
        for (; iter != iter_end_marks; ++iter)
        {
            if (*iter)
            {
                for (uint64_t j = 0; j < block_length && input != end; j++, ++input)
                {
                    res.push_back(*input);
                }
            }
            else
            {
                input += block_length;
            }
        }
        return res;
    }

    template <typename T, typename input_iterator, typename BTLeaves>
    static bool prepare_next_level(bool last_level, input_iterator& input,
                                   input_iterator& end,
                                   input_iterator iter_marked_subsequence,
                                   input_iterator iter_end_marked_subsequence,
                                   uint64_t& length, BTLeaves& leaves,
                                   vector<T>*& r_input)
    {
        if (last_level)
        {
            leaves =
                std::move(BTLeaves(iter_marked_subsequence, iter_end_marked_subsequence));
        }
        else
        {
            input = iter_marked_subsequence;
            end = iter_end_marked_subsequence;
            length = end - input;

            if (r_input)
            {
                r_input->resize(0);
                r_input->insert(r_input->begin(), iter_marked_subsequence,
                                iter_end_marked_subsequence);
                assert(r_input->size() == length);
            }
        }
        return last_level;
    }
};
};
#endif // LIBCDS2_BLOCKTREEHELPER_H
