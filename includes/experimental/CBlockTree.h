//
// Created by Alberto Ordóñez Pereira on 19/05/2017.
//

#ifndef LIBCDS2_CBLOCKTREE_H
#define LIBCDS2_CBLOCKTREE_H
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>
#include <includes/experimental/BlockTree1.h>
#include <includes/experimental/CBlockTreeHelper.h>
#include <includes/experimental/CBlockTreeWrappers.h>
#include <utils/libcdsBasics.h>

namespace cds_static {

template <typename T>
class CBlockTree
{
public:
    typedef typename BlockTreeLevel<T>::input_iterator input_iterator;
    typedef typename vector<T>::size_type size_type;

    CBlockTree()
    {
    }
    ~CBlockTree()
    {
    }

    const StrategyBlockTree& setStrategy() const;

    void build(input_iterator input, input_iterator end, const uint64_t n,
               const uint64_t sigma, StrategyBlockTree&& strategy_,
               const vector<T>* raw_input = nullptr);

    size_type size() const;
    string stats() const;
    uint64_t size_bytes() const;
    T sigma() const;
    const T& operator[](const size_type i) const;
    uint64_t rank(const T& symbol, const size_type& to, const size_type& from = 0) const;
    uint64_t select(const T& symbol, const size_type& i) const;

protected:
    std::vector<CompressedRankSamples<uint32_t>> rnk_boundaries_0;
    std::vector<CompressedRankSamples<uint32_t>> rnk_boundaries_1;
    std::vector<CompressedRankSamples<uint32_t>> rnk_offsets;
    std::vector<CompressedRankSamples<uint32_t>> rnk_offsets_mid;
    std::vector<CompressedMarks> marks;
    std::vector<CompressedPointersBack> pointers;
    std::vector<BlockTreeParams> params;
    BTLeaves<T> leaves;
    int32_t levels;
    T sigma_;
    string stats_;
    uint64_t n;
    StrategyBlockTree strategy;

protected:
    void build_compressed(BlockTreeLevel<T>& bt);
    void compress_pointers_back(BlockTreeLevel<T>& bt);
    void compress_ranks(BlockTreeLevel<T>& bt, input_iterator input,
                        const input_iterator end, const uint64_t aligned_block_length);
    std::vector<
        std::multiset<vector<Block>::const_iterator, CompareBlocksIteratorsFirstOcc>>
    gather_pointed_blocks(const BlockTreeLevel<T>& bt, const vector<uint32_t>& ranks0);
    uint64_t lower_bound(const uint32_t, const T&, const uint64_t, const uint64_t,
                         const uint64_t) const;
};

template <typename T>
const StrategyBlockTree& CBlockTree<T>::setStrategy() const
{
    return this->strategy;
}

template <typename T>
void CBlockTree<T>::build(input_iterator input, input_iterator end, const uint64_t n,
                          const uint64_t sigma, StrategyBlockTree&& strategy_,
                          const vector<T>* raw_input)
{
    this->strategy = strategy_;
    this->levels = this->strategy.levels();
    this->sigma_ = sigma;
    this->n = n;
    uint64_t length = n;
    vector<T>* r_input = const_cast<vector<T>*>(raw_input);
    stringstream stats_string;
    vector<BlockTreeLevel<T>> bts(this->levels);

    for (int32_t i = 0; i < this->levels; ++i)
    {
        uint64_t b_length = this->strategy.block_length(i);
        uint64_t degree = (i > 0) ? this->strategy.block_length(i - 1) /
                                        this->strategy.block_length(i) :
                                    std::ceil(length / b_length);
        this->params.push_back(BlockTreeParams(length, b_length, degree));
        bts[i].build(input, end, length, sigma, b_length, r_input);
        build_compressed(bts[i]);
        compress_ranks(bts[i], input, end,
                       (i == 0) ? bts[i].size() : this->params[i - 1].block_length);
        stats_string << bts[i].stats() << std::endl;
        bts[i].prepare_next_level((i + 1 == this->levels), input, end, length,
                                  this->leaves, r_input);
    }
    this->stats_ = stats_string.str();
}

template <typename T>
typename CBlockTree<T>::size_type CBlockTree<T>::size() const
{
    return this->n;
}

template <typename T>
string CBlockTree<T>::stats() const
{
    stringstream ss;
    size_type total = 0;

    for (auto i = 0; i < this->levels; i++)
    {
        size_type subtotal = 0;
        ss << "Level " << i << endl;

        subtotal += this->marks[i].size_bytes();

        subtotal += this->pointers[i].size_bytes();

        subtotal += this->rnk_boundaries_0[i].size_bytes();

        subtotal += this->rnk_boundaries_1[i].size_bytes();

        subtotal += this->rnk_offsets[i].size_bytes();

        subtotal += this->rnk_offsets_mid[i].size_bytes();

        subtotal += this->params[i].size_bytes();
        ss << "\tPointers size:\t\t" << this->pointers[i].size_bytes() << "\t"
           << this->pointers[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << "\tRank 0s size:\t\t" << this->rnk_boundaries_0[i].size_bytes() << "\t"
           << this->rnk_boundaries_0[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << "\tRank 1s size:\t\t" << this->rnk_boundaries_1[i].size_bytes() << "\t"
           << this->rnk_boundaries_1[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << "\tRank offsets size:\t" << this->rnk_offsets[i].size_bytes() << "\t"
           << this->rnk_offsets[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << "\tRank offsets mid size:\t" << this->rnk_offsets_mid[i].size_bytes()
           << "\t" << this->rnk_offsets_mid[i].size_bytes() * 100.0 / subtotal << " %"
           << endl;
        ss << "\tMarks size:\t\t" << this->marks[i].size_bytes() << "\t"
           << this->marks[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << "\tParams size:\t\t" << this->params[i].size_bytes() << "\t"
           << this->params[i].size_bytes() * 100.0 / subtotal << " %" << endl;
        ss << endl;

        total += subtotal;
    }
    total += leaves.size_bytes();
    ss << "\tLeaves:\t\t" << leaves.size_bytes() << "\t"
       << leaves.size_bytes() * 100.0 / total << " %" << endl;
    ss << "Sum up" << endl;
    ss << "\tSize in bps: " << total * 8.0 / this->n << endl;
    ss << endl;
    return ss.str();
}

template <typename T>
T CBlockTree<T>::sigma() const
{
    return this->sigma_;
}

template <typename T>
uint64_t CBlockTree<T>::size_bytes() const
{
    size_type total = 0;
    for (auto i = 0; i < this->levels; i++)
    {
        total += this->marks[i].size_bytes();
        total += this->pointers[i].size_bytes();
        total += this->rnk_boundaries_0[i].size_bytes();
        total += this->rnk_boundaries_1[i].size_bytes();
        total += this->rnk_offsets[i].size_bytes();
        total += this->rnk_offsets_mid[i].size_bytes();
        total += this->params[i].size_bytes();
    }
    total += leaves.size_bytes();
    return total;
}

template <typename T>
void CBlockTree<T>::build_compressed(BlockTreeLevel<T>& bt)
{
    marks.push_back(CompressedMarks(bt.marks_begin(), bt.marks_end()));
    assert(CBlockTreeHelper::check_correctness_bitmap(bt.marks_begin(), bt.marks_end(),
                                                      marks.back()));
    compress_pointers_back(bt);
}

template <typename T>
void CBlockTree<T>::compress_pointers_back(BlockTreeLevel<T>& bt)
{
    vector<uint32_t> ranks0 =
        BlockTreeHelper::compute_rank_zeroes<uint32_t>(bt.marks_begin(), bt.marks_end());
    vector<uint32_t> block_indices(ranks0.size()); // # of unmarked blocks
    vector<uint32_t> block_offsets(ranks0.size()); // # of unmarked blocks

    for (auto iter = bt.pointers_back_begin(); iter != bt.pointers_back_end(); ++iter)
    {
        for (auto block = iter->second.cbegin(); block != iter->second.cend(); ++block)
        {
            auto position = block->block_pos / bt.get_block_length();
            if (!bt.is_marked(position))
            {
                auto pos = block->first_occ / bt.get_block_length();
                block_indices[ranks0[position] - 1] =
                    pos - ranks0[pos]; // ranks1 from ranks0 -1
                block_offsets[ranks0[position] - 1] =
                    (pos + 1) * bt.get_block_length() - block->first_occ - 1;
            }
        }
    }
    /*cds_static::DirectAccess *indices = new cds_static::DAC(&block_indices[0],
    block_indices.size(), false); cds_static::DirectAccess *offsets = new
    cds_static::DAC(&block_offsets[0], block_offsets.size(), false);*/
    this->pointers.push_back(CompressedPointersBack(block_offsets, block_indices));
}

template <typename T>
void CBlockTree<T>::compress_ranks(BlockTreeLevel<T>& bt, input_iterator input,
                                   const input_iterator end,
                                   const uint64_t aligned_block_length)
{
    uint64_t i = 0;
    uint32_t distinct_values = static_cast<uint32_t>(bt.get_sigma()) + 1;
    vector<vector<uint32_t>> ranks_blocks_0 = vector<vector<uint32_t>>(distinct_values);
    vector<vector<uint32_t>> ranks_blocks_1 = vector<vector<uint32_t>>(distinct_values);
    vector<vector<uint32_t>> ranks_offsets = vector<vector<uint32_t>>(distinct_values);
    vector<vector<uint32_t>> ranks_offsets_mid =
        vector<vector<uint32_t>>(distinct_values);
    vector<uint32_t> ranks0 =
        BlockTreeHelper::compute_rank_zeroes<uint32_t>(bt.marks_begin(), bt.marks_end());
    uint64_t number_of_zeros = ranks0.back();
    vector<uint32_t> counter(distinct_values);

    auto ranks_map = gather_pointed_blocks(bt, ranks0);

    for (auto& v : ranks_offsets)
    {
        v = vector<uint32_t>(number_of_zeros);
    }
    for (auto& v : ranks_offsets_mid)
    {
        v = vector<uint32_t>(number_of_zeros);
    }

    for (auto iter = bt.marks_begin(); iter != bt.marks_end(); ++iter, ++i)
    {
        auto index = i * bt.get_block_length();

        if (index % aligned_block_length == 0)
        { // if the block boundary aligns with the previous block_length
            for (auto& it : counter)
                it = 0;
        }
        auto lim = std::min(static_cast<uint64_t>(bt.size()),
                            static_cast<uint64_t>((i + 1) * bt.get_block_length()));
        if (*iter)
        {
            for (auto j = 0; j < distinct_values; j++)
                ranks_blocks_1[j].push_back(counter[j]);
            auto iter_blocks = ranks_map[i - ranks0[i]].begin();
            auto iter_blocks_end = ranks_map[i - ranks0[i]].end();
            // Blocks are iterated in order of first_occ
            for (; iter_blocks != iter_blocks_end; ++iter_blocks)
            {
                for (; index < (*iter_blocks)->first_occ; index++, ++input)
                {
                    counter[static_cast<uint32_t>(*input)]++;
                }
                auto unmarked_idx =
                    ranks0[(*iter_blocks)->block_pos / bt.get_block_length()] - 1;
                assert(unmarked_idx < number_of_zeros);
                BlockTreeHelper::clone_counters<T, uint32_t>(
                    input, input + (lim - index), counter, ranks_blocks_1, ranks_offsets,
                    ranks_offsets_mid, unmarked_idx);
            }
        }
        else
        {
            for (auto j = 0; j < distinct_values; j++)
                ranks_blocks_0[j].push_back(counter[j]);
        }
        for (; index < lim; index++, ++input)
        {
            counter[static_cast<uint32_t>(*input)]++;
        }
    }

    this->rnk_boundaries_0.push_back(CompressedRankSamples<uint32_t>(ranks_blocks_0));
    this->rnk_boundaries_1.push_back(CompressedRankSamples<uint32_t>(ranks_blocks_1));
    this->rnk_offsets.push_back(CompressedRankSamples<uint32_t>(ranks_offsets));
    this->rnk_offsets_mid.push_back(CompressedRankSamples<uint32_t>(ranks_offsets_mid));
    assert(CBlockTreeHelper::test_ranks(bt.marks_begin(), bt.marks_end(),
                                        this->rnk_boundaries_0.back(),
                                        this->rnk_boundaries_1.back(), distinct_values,
                                        bt.get_block_length(), aligned_block_length));
}
/**
 * Returns, for each block, which blocks are pointing to it.
 * @tparam T BlockTree inner type
 * @param bt BlockTree (uncompressed)
 * @param ranks0 vector that explicitly stores the rank0 for each position of the bitmap
 * of marks
 * @return
 */
template <typename T>
std::vector<std::multiset<vector<Block>::const_iterator, CompareBlocksIteratorsFirstOcc>>
CBlockTree<T>::gather_pointed_blocks(const BlockTreeLevel<T>& bt,
                                     const vector<uint32_t>& ranks0)
{
    std::vector<
        std::multiset<vector<Block>::const_iterator, CompareBlocksIteratorsFirstOcc>>
        ranks_map(ranks0.size() - ranks0.back() + 1);
    for (auto iter = bt.pointers_back_begin(); iter != bt.pointers_back_end(); ++iter)
    {
        for (auto block = iter->second.cbegin(); block != iter->second.cend(); ++block)
        {
            if (!bt.is_marked(block->block_pos / bt.get_block_length()))
            {
                auto pos = block->first_occ / bt.get_block_length();
                pos = pos - ranks0[pos];
                ranks_map[pos].insert(block);
            }
        }
    }
    return ranks_map;
}

template <typename T>
const T& CBlockTree<T>::operator[](const size_type position) const
{
    size_type pos = position;
    for (int32_t i = 0; i < this->levels; i++)
    {
        auto block = pos / this->params[i].block_length;
        if (!this->marks[i][block])
        {
            auto umk_idx = this->marks[i].rank0(block) - 1;
            auto ref_blk_idx = this->pointers[i].indice(umk_idx);
            pos = pos % this->params[i].block_length;
            pos += (ref_blk_idx + 1) * this->params[i].block_length - 1;
            pos -= this->pointers[i].offset(umk_idx);
        }
        else
        {
            pos = this->marks[i].rank1(block - 1) * this->params[i].block_length +
                  (pos % this->params[i].block_length);
        }
    }
    return this->leaves[pos];
}

template <typename T>
uint64_t CBlockTree<T>::rank(const T& symbol, const size_type& to,
                             const size_type& from) const
{
    size_type pos = to;
    uint64_t acm_rnk = 0;
    uint64_t block_start = 0;
    for (int32_t level = 0; level < this->levels; level++)
    {
        auto block = pos / this->params[level].block_length;
        if (this->marks[level][block])
        {
            auto rank1 = this->marks[level].rank1(block) - 1;
            block_start = this->params[level].block_length * rank1;
            pos = block_start + (pos % this->params[level].block_length);
            acm_rnk += this->rnk_boundaries_1[level][symbol][rank1];
        }
        else
        {
            auto umk_idx = this->marks[level].rank0(block) - 1;
            auto ref_blk_idx = this->pointers[level].indice(umk_idx);
            auto off_ref = this->pointers[level].offset(umk_idx);
            auto off_blk = pos % this->params[level].block_length;
            acm_rnk += this->rnk_boundaries_0[level][symbol][umk_idx];

            if (off_blk > off_ref)
            { // pos in second block
                block_start = (ref_blk_idx + 1) * this->params[level].block_length;
                acm_rnk += this->rnk_offsets_mid[level][symbol][umk_idx];
                pos = off_blk - off_ref - 1;
            }
            else
            {
                acm_rnk -= this->rnk_offsets[level][symbol][umk_idx];
                block_start = this->params[level].block_length * ref_blk_idx;
                pos = this->params[level].block_length - off_ref + off_blk - 1;
            }
            pos = block_start + pos;
        }
    }
    return acm_rnk + this->leaves.rank(symbol, pos, block_start);
}
/**
 * Returns the block that contains select(symbol, s).
 * @tparam T
 * @param level Level of the block tree
 * @param symbol Symbol of interest
 * @param s Value of select [1...max_number_of_symbol_in_the_input]
 * @param from: initial position in the bitmap to inspect marks[from,...
 * @param to: final position in the bitmap to inspect marks[from, to) (open left interval)
 * @return Returns the block that contains select(symbol, s).
 */
template <class T>
uint64_t CBlockTree<T>::lower_bound(const uint32_t level, const T& symbol,
                                    const uint64_t s, const uint64_t from,
                                    const uint64_t to) const
{
    int64_t count = to - from;
    uint64_t i = from;
    uint64_t p = from;
    uint64_t step = 0;
    while (count > 0)
    {
        step = count / 2;
        i = p + step;
        auto v =
            (this->marks[level][i]) ?
                this->rnk_boundaries_1[level][symbol][this->marks[level].rank1(i) - 1] :
                this->rnk_boundaries_0[level][symbol][this->marks[level].rank0(i) - 1];
        if (v < s)
        {
            p = ++i;
            count -= step + 1;
        }
        else
        {
            count = step;
        }
    }
    assert(p >= from && p <= to);
    return p - 1;
}

template <class T>
uint64_t CBlockTree<T>::select(const T& symbol, const size_type& i) const
{
    uint64_t acm_pos = 0;
    uint64_t block_start = 0;
    size_type ref_blk = 0;
    size_type sel = i;

    for (int32_t level = 0; level < this->levels; ++level)
    {
        ref_blk *= this->params[level].degree;
        // if we pad, we avoid the min computation
        auto block = lower_bound(
            level, symbol, sel, ref_blk,
            std::min(static_cast<size_type>(ref_blk + this->params[level].degree),
                     this->marks[level].size()));

        acm_pos += block * this->params[level].block_length - block_start;

        if (this->marks[level][block])
        {
            auto rank1 = this->marks[level].rank1(block) - 1;
            sel -= this->rnk_boundaries_1[level][symbol][rank1];
            block_start = rank1 * this->params[level].block_length;
            ref_blk = rank1;
        }
        else
        {
            auto umk_idx = this->marks[level].rank0(block) - 1;
            auto rnk_mid = this->rnk_offsets_mid[level][symbol][umk_idx];
            ref_blk = this->pointers[level].indice(umk_idx);
            sel -= this->rnk_boundaries_0[level][symbol][umk_idx];

            if (sel > rnk_mid)
            {
                sel -= rnk_mid;
                assert(sel > 0);
                ref_blk += 1;
                block_start = ref_blk * this->params[level].block_length;
                acm_pos += this->pointers[level].offset(umk_idx) + 1;
            }
            else
            {
                acm_pos -= this->params[level].block_length -
                           this->pointers[level].offset(umk_idx) - 1;
                sel += this->rnk_offsets[level][symbol][umk_idx];
                block_start = ref_blk * this->params[level].block_length;
            }
        }
    }
    return acm_pos +
           this->leaves.select(symbol, sel,
                               block_start + this->params[this->levels - 1].block_length,
                               block_start);
}
};
#endif // LIBCDS2_CBLOCKTREE_H
