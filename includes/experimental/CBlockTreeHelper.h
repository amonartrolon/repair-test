//
// Created by Alberto Ordóñez Pereira on 19/05/2017.
//

#ifndef LIBCDS2_CBLOCKTREEHELPER_H
#define LIBCDS2_CBLOCKTREEHELPER_H
#include <fstream>
#include <iostream>
#include <bitsequence/BitSequence.h>
#include <includes/experimental/CBlockTreeWrappers.h>

namespace cds_static {

class CBlockTreeHelper
{
public:
    static bool check_correctness_bitmap(vector<bool>::const_iterator iter,
                                         const vector<bool>::const_iterator& end,
                                         const CompressedMarks& marks)
    {
        uint64_t i = 0;
        for (; iter != end; ++iter, ++i)
        {
            assert(*iter == marks[i]);
        }
        assert(i == marks.size());
        return true;
    }

    static bool test_ranks(vector<bool>::const_iterator iter,
                           const vector<bool>::const_iterator& end,
                           const CompressedRankSamples<uint32_t>& ranks_0,
                           const CompressedRankSamples<uint32_t>& ranks_1,
                           const uint32_t sigma, const uint64_t block_length,
                           const uint64_t block_length_prev)
    {
        auto index0 = 0UL, index1 = 0UL, i = 0UL;
        for (; iter != end; ++iter, ++i)
        {
            if (*iter)
            {
                if (((i * block_length) % block_length_prev) == 0)
                {
                    for (auto s = 0; s < sigma; s++)
                        assert(ranks_1[s][index1] == 0);
                }
                index1++;
            }
            else
            {
                if (((i * block_length) % block_length_prev) == 0)
                {
                    for (auto s = 0; s < sigma; s++)
                        assert(ranks_0[s][index0] == 0);
                }
                index0++;
            }
        }
        return true;
    }
};
};
#endif // LIBCDS2_CBLOCKTREEHELPER_H
