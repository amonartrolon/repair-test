#ifndef _WAVELET_MATRIX_HUFFMAN_UTILS_H_
#define _WAVELET_MATRIX_HUFFMAN_UTILS_H_

#include <cstdlib>

namespace cds_static{


  class WaveletMatrixHuffUtils{

    static void updateTableCodigos(uint **tableCodigos1, uint **tableCodigos2, uint *maxCodes, uint *nCodesAtLevel, uint maxLength, uint minLength, uint level, uint group){
      int i;
      for (i=(int)group;i<=(int)maxLength;i++){
        if (nCodesAtLevel[i]==0){
          tableCodigos2[level][i]--;
          tableCodigos2[level][i+1]++;
          continue;
        }
        if (tableCodigos2[level][i]==maxCodes[i]){
          tableCodigos2[level][i+1]++;
        }else{
          break;
        }
      }
      int change = --i;
      //~ if (change>=minLength){debug++; cout << change << ", #: " << cuantos << endl;cuantos=0;}
      for (int j=i;j>=(int)minLength;j--){
        if (nCodesAtLevel[j]==0) continue;
        tableCodigos2[level][j]=tableCodigos1[change][j];
      }
    }


    static uint buildCodigos(uint minLength, uint maxLength, uint *nCodesAtLevel, uint **codes, uint **invNegCodes, uint **bmpCodes, uint **lastCode){
      uint ncodes = 0;
      (*bmpCodes) = new uint[maxLength+1];

      (*bmpCodes) = new uint[((1<<maxLength)+W-1)/W];
      for (uint i=0;i<((1<<maxLength)+W-1)/W;i++) (*bmpCodes)[i]=0;


      uint **tableCodes = new uint*[maxLength+2];
      uint **tableCodes2 = new uint*[maxLength+2];
      for (uint i=0;i<=maxLength+1;i++){
        tableCodes[i]=new uint[maxLength+2];
        tableCodes2[i]=new uint[maxLength+2];
      }
      for (uint i=0;i<=maxLength+1;i++){
        for (uint j=0;j<=maxLength;j++){
          tableCodes[i][j]=0;
          tableCodes2[i][j]=0;
        }
      }

      (*lastCode) = new uint[maxLength+1];
      uint *maxCode;
      maxCode = new uint[maxLength+1];
      uint from=0;
      for (uint i=0;i<=maxLength;i++){
        if (nCodesAtLevel[i]!=0){
          maxCode[i] = (1<<(i-from));
          from=i;
        }
      }

      for (uint i=minLength;i<=maxLength;i++){ncodes+=nCodesAtLevel[i];}

      (*codes) = new uint[ncodes];
      (*invNegCodes) = new uint[ncodes];
      uint code;
      uint pcodes=0;

      for (uint len = minLength; len <= maxLength; len++){
        //~ cout << "len: " << len << ", nCodes: " << nCodesAtLevel[len]<< endl;
        for (uint i=0;i<nCodesAtLevel[len];i++){
          uint j=0;
          code=0;
          for (uint k=minLength;k<=len;k++){
            if (nCodesAtLevel[k]==0) continue;
            assert(tableCodes2[len][k]>=tableCodes[k][k]);
            code = code | (tableCodes2[len][k]<<j);
            j=k;
          }
          //cuantos++;

          bit_set((*bmpCodes), code);

          (*codes)[pcodes] = ~(invertWord(code) >> (32 -len)) ;
          (*invNegCodes)[pcodes] = code;
          if (pcodes>1){
            assert((*invNegCodes)[pcodes]>(*invNegCodes)[pcodes-1]);
          }
          pcodes++;
          tableCodes2[len][minLength]++;
          updateTableCodigos(tableCodes,tableCodes2,maxCode,nCodesAtLevel,maxLength,minLength,len,minLength);
        }

          (*lastCode)[len]=(*invNegCodes)[pcodes-1];

        for (uint i=minLength;i<=len;i++){
          tableCodes[len][i] = tableCodes2[len][i];
          tableCodes2[len+1][i] = tableCodes2[len][i];
        }
      }
      for (uint i=0;i<=maxLength+1;i++){
        delete [] tableCodes[i];
        delete [] tableCodes2[i];
      }
      delete [] tableCodes;
      delete [] tableCodes2;
      delete [] maxCode;
      assert(pcodes==ncodes);
      return pcodes;
    }

    static uint buildCodigos2(uint minLength, uint maxLength, uint *nCodesAtLevel, uint **codes, uint **invNegCodes, uint **bmpCodes, uint **lastCode){
      uint ncodes = 0;
      (*bmpCodes) = new uint[maxLength+1];

      (*bmpCodes) = new uint[((1<<maxLength)+W-1)/W];
      for (uint i=0;i<((1<<maxLength)+W-1)/W;i++) (*bmpCodes)[i]=0;


      uint **tableCodes = new uint*[maxLength+2];
      uint **tableCodes2 = new uint*[maxLength+2];
      for (uint i=0;i<=maxLength+1;i++){
        tableCodes[i]=new uint[maxLength+2];
        tableCodes2[i]=new uint[maxLength+2];
      }
      for (uint i=0;i<=maxLength+1;i++){
        for (uint j=0;j<=maxLength;j++){
          tableCodes[i][j]=0;
          tableCodes2[i][j]=0;
        }
      }

      (*lastCode) = new uint[maxLength+1];
      uint *maxCode;
      maxCode = new uint[maxLength+1];
      uint from=0;
      for (uint i=0;i<=maxLength;i++){
        if (nCodesAtLevel[i]!=0){
          maxCode[i] = (1<<(i-from));
          from=i;
        }
      }

      for (uint i=minLength;i<=maxLength;i++){ncodes+=nCodesAtLevel[i];}

      (*codes) = new uint[ncodes];
      (*invNegCodes) = new uint[ncodes];
      uint code;
      uint pcodes=0;

      for (uint len = minLength; len <= maxLength; len++){
        //~ cout << "len: " << len << ", nCodes: " << nCodesAtLevel[len]<< endl;
        for (uint i=0;i<nCodesAtLevel[len];i++){
          uint j=0;
          code=0;
          for (uint k=minLength;k<=len;k++){
            if (nCodesAtLevel[k]==0) continue;
            assert(tableCodes2[len][k]>=tableCodes[k][k]);
            code = code | (tableCodes2[len][k]<<j);
            j=k;
          }
          //cuantos++;

          bit_set((*bmpCodes), code);

          (*codes)[pcodes] = ~(invertWord(code) >> (32 -len)) ;
          (*invNegCodes)[pcodes] = code;
          if (pcodes>1){
            assert((*invNegCodes)[pcodes]>(*invNegCodes)[pcodes-1]);
          }
          pcodes++;
          tableCodes2[len][minLength]++;
          updateTableCodigos(tableCodes,tableCodes2,maxCode,nCodesAtLevel,maxLength,minLength,len,minLength);
        }

          (*lastCode)[len]=(*invNegCodes)[pcodes-1];

        for (uint i=minLength;i<=len;i++){
          tableCodes[len][i] = tableCodes2[len][i];
          tableCodes2[len+1][i] = tableCodes2[len][i];
        }
      }
      for (uint i=0;i<=maxLength+1;i++){
        delete [] tableCodes[i];
        delete [] tableCodes2[i];
      }
      delete [] tableCodes;
      delete [] tableCodes2;
      delete [] maxCode;
      assert(pcodes==ncodes);
      return pcodes;
    }
  };
};
#endif
