/* BlockTreeHelper.h
 * Copyright (C) 2016, Alberto Ordóñez, all rights reserved.
 *
 * e-mail: alberto.ordonez@udc.es
 *
 * Block Tree Helper.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef _BLOCK_TREE_HELPER_H_
#define _BLOCK_TREE_HELPER_H_
#include <iostream>

using namespace cds_utils;

namespace cds_static {
    template<typename T, class Leaves, class ExternalMod> class BlockTree;
    class BlockTreeHelper{

    public:
      template<typename T, class Leaves, class ExternalMod>  static int BufferSetUp(const string &filebin, const string &filebout,
                                                  string &buffer1, string &buffer2){
        buffer1 = filebin;
        if (buffer1.empty()){
            TemporaryFile tmpFile1;
            buffer1 = tmpFile1.getFileName();
            ifstream in(buffer1);
            if (!in.good()){
                in.close();
                return 0;
            }
        }
        buffer2 = filebout;
        if (buffer2.empty()){
            //generate tmp files for the buffers
            TemporaryFile tmpFile;
            buffer2 = tmpFile.getFileName();
            ifstream in(buffer2);
            if (!in.good()){
                in.close();
                return 0;
            }
        }
        assert(buffer1!=buffer2);
        return buffer1!=buffer2;
      };

      static int BufferTearDown(const string &buffer1, const string &buffer2){
        remove(buffer1.c_str());
        remove(buffer2.c_str());
        return 1;
      }

      template<typename T, class Leaves, class ExternalMod>  static int InputSizeAndChecks(const string &filein, size_t &n){
         int ret = 1;
         ifstream in_f(filein);
         in_f.seekg(0, ios_base::end);
         n = in_f.tellg() / sizeof(T);
         if(in_f.tellg() % sizeof(T) != 0){
              ret = 0;
         }
         in_f.close();
       }

       template<typename T, class Leaves, class ExternalMod> static int
          InitializeBlockTreeStructures(BlockTree<T,Leaves,ExternalMod> &bt){
         bt.blockLengthLevel.reserve(bt.max_levels_bt);
         bt.degreeLevel.reserve(bt.max_levels_bt);

         if (bt.bsb_marks == nullptr) {
            bt.bsb_marks = new BitSequenceBuilderPlain(32);
         }
         if (bt.strategySplitBlocks == nullptr){}

         bt.marks.reserve(bt.max_levels_bt);
         for (auto &m: bt.marks){
             m = nullptr;
         }
         bt.pointersBack.reserve(bt.max_levels_bt);
         bt.offsetsBack.reserve(bt.max_levels_bt);
         return 1;
       }
    };
};

#endif
