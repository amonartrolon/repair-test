/* BlockTree.h
 * Copyright (C) 2015, Alberto Ordóñez, all rights reserved.
 *
 * e-mail: alberto.ordonez@udc.es
 *
 * Block Graph implementation. Initially, a block graph should only permit to
 * access a given position or a range of positions in a sequence. If one
 * wants to carry out rank/select operations, on must to take samples of ranks
 * until some positions (adding a \sigma overhead). Thus, we have implemented
 * the Block Graph using a template pattern: we implemented the parsing algorithm
 * and we left several functions unimplemented (or with a default behavior). For
 * instance, the functions that store the rank samplings must not be implemented by
 * a BlockTree but by a SequenceBlockTree. If a function has no sense for a class,
 * the class provides a default behaviour (do nothing for instance).
 *
 * SequenceBlockTree<T,Leaves,ExternalMod>. It adds rank/select capabilities to a BlockTree.
 * It extends a BlockTree and implements those functions necessary to
 * store and deal with rank samples. It also extends from class Sequence,
 * so that we can use a SequenceBlockTree<T,Leaves,ExternalMod> where a Sequence is needed, that
 * is, where rank/select/access functionallities are necessary.
 *
 * References:
 * [1]: Belazzougui, D.; Gagie, T.; Gawrychowski, P.; Kärkkäinen, J.; Ordóñez, A.; Puglisi, S.J.; Tabei, Y.:
 *      "Queries on LZ-Bounded Encodings", en Proceedings of the 2015 Data Compression Conference (DCC 2015),
 *      IEEE Computer Society, Salt Lake City, Utah (Estados Unidos), 2015, pp. 83-92.
 *
 * Seq
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef _BLOCK_TREE_H_
#define _BLOCK_TREE_H_

#include <direct_access/DirectAccess.h>
#include <bitsequence/BitSequenceBuilder.h>
#include <sequence/SequenceLZBLockHelper.h>
#include <vector>
#include <sequence/BlockGraphUtils.h>
#include <utils/HashTableBG.h>
#include <sequence/Sequence.h>
#include <utils/StringUtils.h>
#include <utils/SparseSuffixSort.h>
#include <utils/Logger.h>
#include <unordered_map>
#include <sequence/BlockTreeHelper.h>
#include <utils/RabinKarp.h>
#include <utils/timeMeasure.h>

#ifndef BLOCKTREE_HDR
#define BLOCKTREE_HDR 1
#endif
#ifndef SEQUENCE_BLOCKTREE_HDR
#define SEQUENCE_BLOCKTREE_HDR 2
#endif
#ifndef BLOCKTREE_INDEX_HDR
#define BLOCKTREE_INDEX_HDR 3
#endif
/**
* Default paremeters.
* max_levels_bt: máximun number of leaves of a block graph
*/

using namespace cds_utils;

namespace cds_static {

    template<typename T,
            class Leaves,
            class ExternalMod> class BlockTree {

    friend class BlockTreeHelper;

    public:
        //constructor
        BlockTree(StrategySplitBlock *s);
        virtual void build(string &inFile, bool can_delete=false);
        /**
        * @param outStream: stream to write out the data structure. It must be opened if it is
        not empty. The data structure is written level by level, freeing the space the data
        structures take after written them.
        */
        virtual BlockTree& setOutStream(string &outFile);
        virtual BlockTree& setMaxV(T maxv);
        virtual BlockTree& setBufferInputReadSize(ulong inputBufferSize);
        virtual BlockTree& setBufferOutputReadSize(ulong inputBufferSize);
        virtual BlockTree& setBufferInName(const string &tmpFileNameBufferIn);
        virtual BlockTree& setBufferOutName(const string &tmpFileNameBufferOut);
        virtual BlockTree& setLoggFile(const string &logger_file);
        virtual BlockTree& setMarksBuilder(BitSequenceBuilder *bsb);
        //destructor
        virtual ~BlockTree();

        StrategySplitBlock *getStrategy();
        virtual ulong getLength();
        T getSigma();
        //Save and load functions
        virtual void save(ofstream & fp) const;
        static BlockTree* load(ifstream & fp);

        //access operations
        virtual uint operator[](size_t pos) const;
        virtual size_t getSize() const;
        virtual uint getHeader() const;

        static constexpr int max_levels_bt = 32;
        static constexpr int default_max_sigma = 255;
        static constexpr long long default_buffer_write_size = 100000000;
        static constexpr long long default_buffer_read_size = 100000000;

    protected:
        ExternalMod external;
        size_t n;
        T max_v;//the actual value of max_v (the max value found in the input stream)
        T min_v;//the min value found in the input stream
        T max_possible_v;
        ofstream *output;
        long bufferReadSize;
        long bufferWriteSize;
        int firstLevel;
        string tmpFileBufferIn;
        string tmpFileBufferOut;

        BitSequenceBuilder *bsb_marks;
        vector<BitSequence *> marks;
        vector<DirectAccess *> pointersBack;
        vector<DirectAccess *> offsetsBack;
        vector<ulong> blockLengthLevel;
        vector<ulong> degreeLevel;
        vector<uint> leavesLZB;
        Leaves *seq_leaves;
#ifdef MYDEBUG
        Sequence *seq_tests;
#endif

        cds_utils::Logger<string> logger;
        vector<ulong> positions;
        BlockTree();
        StrategySplitBlock *strategySplitBlocks;

        T accessLeaves(long pos) const;
        /*Internal construction functions*/
        ulong run_build(T *&input, ulong n, int level, string &buffer1, string &buffer2);
        void initializeListOfBlocks(unordered_map<ulong, ulong> &hash,
                                const RabinKarp<T> &rk, BlockContainer &listOfBlocks,
                                T *input, const uint64_t start, const uint64_t end,
                                const int level, vector<bool> &plainMarks, string &nameBufferRead);
        ulong CompressMarks(uint level, vector<bool> &plainMarks);
        ulong CompressBG(uint level,vector<uint> &pointersUnmarked,
                              vector<uint> &offsetsUnmarked);
        virtual void indexLeaves(string &inputBuffer,uint level,
                                  const vector<bool> &boundaries);
        void PostProcessMarks(uint level, vector<bool> &plainMarks,
                              ulong nBlocks, BlockContainer &listOfBlocks);
                              // vector<pair<ulong,ulong>> &blockAndPointer);
        inline virtual void updateBlockAndPointer(ulong blockId, ulong firstOcc,
                                                  vector<pair<ulong,ulong>> &blockAndPointers){}
        /**
        * Default behaviour of functions related to rank operations is set to void. If you want to support rank/select operations,
        * you may extend this class and override that functions.
        */
        virtual ulong storePointersBack(T *&input, uint64_t n, int level,
                                        BlockContainer &listOfBlocks,
                                        vector<uint> &pointersUnmarked,
                                        vector<uint> &offsetsUnmarked,
                                        string &bufferRead, string &bufferWrite);
        // virtual ulong buildCompressRank(uint level, vector<uint> *ranksBlocks,
        //                                 vector<uint> *ranksOffsets,
        //                                 const vector<bool> &){return 0ul;}
        // inline virtual void incrementCounterRank(uint level, ulong i,
        //                                           ulong blockLength,
        //                                           ulong blockLengthPreviousLevel,
        //                                           vector<uint> *ranksBlocks,
        //                                           vector<ulong> &counter){}
        // virtual void addToBlockAndPointer(ulong blockId, ulong firstOcc,
        //                                   vector<pair<ulong,ulong>> &blockAndPointer){}

        //internal Save and Load

        void saveLevel(ofstream & fp, int level, bool saveLeaves) const;
        BlockTree<T,Leaves,ExternalMod>* loadByLevels(ifstream & fp);

        //deleter
        void deleteLevel(uint level);
        virtual uint getPointerBack(uint level, size_t block,
                                    size_t &unmarked_index,
                                    size_t &block_index,
                                    size_t &block_pos) const;
    };


    /***********************************************************************************/
    /***********************************************************************************/
    /*********************  BLOCK GRAPH IMPLEMENTATION  ********************************/
    /***********************************************************************************/
    /***********************************************************************************/
    template<typename T, class Leaves, class ExternalMod>  BlockTree<T,Leaves,ExternalMod>::BlockTree(){
        n = 0UL;
    }
    template<typename T, class Leaves, class ExternalMod> ulong  BlockTree<T,Leaves,ExternalMod>::getLength(){
        return n;
    }
    template<typename T, class Leaves, class ExternalMod>T  BlockTree<T,Leaves,ExternalMod>::getSigma(){
        return max_v+1;
    }
    template<typename T, class Leaves, class ExternalMod>StrategySplitBlock*  BlockTree<T,Leaves,ExternalMod>::getStrategy(){
        return strategySplitBlocks;
    }
    template<typename T, class Leaves, class ExternalMod>uint  BlockTree<T,Leaves,ExternalMod>::getHeader() const{
        return static_cast<uint>(BLOCKTREE_HDR);
    }

    template<typename T, class Leaves, class ExternalMod>size_t
      BlockTree<T,Leaves,ExternalMod>::getSize() const{
        //in case of binary alphabets we only consider
        //the samplings for one out of the two symbols
        size_t total = 0;
        total += sizeof(BitSequence*)*(this->strategySplitBlocks->getNLevels()-1);
        for (uint i=firstLevel;i<this->strategySplitBlocks->getNLevels()-1;i++)
            total+=marks[i]->getSize();
        total+= sizeof(DirectAccess*)*(this->strategySplitBlocks->getNLevels()-1);
        for (uint i=firstLevel;i<this->strategySplitBlocks->getNLevels()-1;i++)
            total+=pointersBack[i]->getSize();
        total+= sizeof(DirectAccess*)*(this->strategySplitBlocks->getNLevels()-1);
        for (uint i=firstLevel;i<this->strategySplitBlocks->getNLevels()-1;i++)
            total+=offsetsBack[i]->getSize();
        total+= sizeof(uint)*(this->strategySplitBlocks->getNLevels()-1);
        total+=seq_leaves->getSize();
        return total;
    }
    //destructor
    template<typename T, class Leaves, class ExternalMod>
      BlockTree<T,Leaves,ExternalMod>::~BlockTree() {
        if (output) delete output;
        for (uint i=firstLevel;i<this->strategySplitBlocks->getNLevels()-1;i++){
            deleteLevel(i);
        }
        delete strategySplitBlocks;
        if (seq_leaves) delete seq_leaves;
        for (auto &it : marks){
            if (it) delete it;
        }
    }
    //constructor
    template<typename T, class Leaves, class ExternalMod>
      BlockTree<T,Leaves,ExternalMod>::BlockTree(StrategySplitBlock* st):strategySplitBlocks(st) {
        if (strategySplitBlocks->getNLevels()>= max_levels_bt){
            cerr << "The number of levels must be smaller than " << max_levels_bt << endl;
            exit(0);
        }
        strategySplitBlocks = st;
        firstLevel          = -1;
        bufferReadSize      = default_buffer_read_size;
        bufferWriteSize     = default_buffer_write_size;
        max_possible_v      = default_max_sigma;
        output              = nullptr;
        bsb_marks           = new BitSequenceBuilderPlain(32);
        seq_leaves          = nullptr;
    }
    template<typename T, class Leaves, class ExternalMod>BlockTree<T,Leaves,ExternalMod>
      &BlockTree<T,Leaves,ExternalMod>::setMarksBuilder(BitSequenceBuilder *bsb){
        if (this->bsb_marks)
          delete this->bsb_marks;
        this->bsb_marks = bsb;
        return *this;
    }
    template<typename T, class Leaves, class ExternalMod>BlockTree<T,Leaves,ExternalMod>
      &BlockTree<T,Leaves,ExternalMod>::setOutStream(string &outFile){
        if (!outFile.empty()) {
            output = new ofstream(outFile);
        }
        return *this;
    }
    //@deprecated
    template<typename T, class Leaves, class ExternalMod>BlockTree<T,Leaves,ExternalMod>&  BlockTree<T,Leaves,ExternalMod>::setMaxV(T maxValue){
        this->max_possible_v = maxValue;
        return *this;
    }
    template<typename T, class Leaves, class ExternalMod>BlockTree<T,Leaves,ExternalMod>&  BlockTree<T,Leaves,ExternalMod>::setBufferInputReadSize(ulong inputBufferSize) {
        this->bufferReadSize = inputBufferSize;
        return *this;
    }
    template<typename T, class Leaves, class ExternalMod>BlockTree<T,Leaves,ExternalMod>&  BlockTree<T,Leaves,ExternalMod>::setBufferOutputReadSize(ulong outputBufferSize){
        this->bufferWriteSize = outputBufferSize;
        return *this;
    }
    template<typename T,class Leaves, class ExternalMod> BlockTree<T,Leaves,ExternalMod>& BlockTree<T,Leaves,ExternalMod>::setBufferInName(const string &tmpFileNameBufferIn){
        this->tmpFileBufferIn = tmpFileNameBufferIn;
        return *this;
    }
    template<typename T,class Leaves, class ExternalMod> BlockTree<T,Leaves,ExternalMod>& BlockTree<T,Leaves,ExternalMod>::setBufferOutName(const string &tmpFileNameBufferOut) {
        this->tmpFileBufferOut = tmpFileNameBufferOut;
        return *this;
    }
    template<typename T,class Leaves, class ExternalMod> BlockTree<T,Leaves,ExternalMod>& BlockTree<T,Leaves,ExternalMod>::setLoggFile(const string &logger_file) {
        this->logger.setFile(logger_file);
        return *this;
    }
    template<typename T, class Leaves, class ExternalMod>void  BlockTree<T,Leaves,ExternalMod>::build(string &inFile, bool can_delete) {

        //can_delete = false;
        /**Check if the files for input and output buffers were set. If don't, create
        *  a temporaty file*/
        string bufferInFirstIter, buffer1, buffer2;
        if (!BlockTreeHelper::BufferSetUp(this->tmpFileBufferIn, this->tmpFileBufferOut, buffer1, buffer2)){
          logger.ERROR("Couldn't build the input buffers");
          return;
        }
        bufferInFirstIter   = buffer1;
        buffer1             = inFile; // at the first iter the read buffer is the input file
        T *input            = nullptr; //TODO: remove
        BlockTreeHelper::InputSizeAndChecks(buffer1, this->n);
        BlockTreeHelper::InitializeBlockTreeStructures(this);
        uint64_t new_length = n;
        for (int32_t level=0; new_length!=0; level++) {
            this->logger.INFO("Running build algorithm for level " +std::to_string(level),"BlockTree");
            new_length = run_build(input, new_length, level, buffer1, buffer2);
            if (output && output->good()) {
                this->logger.INFO("Saving level "+std::to_string(level),"BlockTree");
                saveLevel(*output, level, strategySplitBlocks->stop(level));
            }
            if (level==0){
                buffer1=buffer2;
                buffer2=bufferInFirstIter;
            }else {
                swap(buffer1, buffer2);
            }
        }
#ifdef MYDEBUG
        delete [] input;
#endif
        //remove buffers
        if (!BlockTreeHelper::BufferTearDown(buffer1, buffer2)) return;
    }

    template<typename T, class Leaves, class ExternalMod> ulong  BlockTree<T,Leaves,ExternalMod>::run_build(T *&input, ulong n, int level, string &buffer1, string &buffer2){

        vector<bool> plainMarks;
        vector<uint> pointersUnmarked;
        vector<uint> offsetsUnmarked;
        ulong pointer_new_input = n;

#ifdef MYDEBUG
        vector<T> sequence_tests(n);
        for (size_t i=0;i<n;i++)
            sequence_tests[i]=input[i];
#endif
        ulong blockLengthPreviousLevel = ((level==0) || (firstLevel==-1) || blockLengthLevel[level-1]==0)?n+1:blockLengthLevel[level-1];
        if (strategySplitBlocks->stop(level)){
            blockLengthLevel[level] = blockLengthLevel[level-1];
            // testSegmentsIndex(level, boundaries, n);
            // indexLeaves(buffer1,level,boundaries);
            pointer_new_input = 0;
        }else {
            this->logger.INFO("level: "+std::to_string(level), "BlockTree");
            strategySplitBlocks->computeBlockLength(level, blockLengthLevel, degreeLevel, n);
            assert(max_possible_v>0);
            RabinKarp<T> rabinKarp(max_possible_v, blockLengthLevel[level]);
            //TODO: cannot make such an assumption about max_possible_v
            uint64_t nBlocks = n / blockLengthLevel[level] + ((n % blockLengthLevel[level]) ? 1 : 0);
            //generally, it is convenient to uncomment the following lines (or define the arity in a different way) since
            //in the first two levels every block is marked. It doesn't affect the space performance nor the query time since,
            //later in the code, if all the blocks are marked we skip that level.
            if (level < 0) {
                pointer_new_input = 0;
            } else {
                this->logger.INFO("Creating the hash table","BLockGraph");
                plainMarks.assign(nBlocks, false);
                unordered_map<ulong, ulong> hash(nBlocks*1.35);
                vector<bool> symbolFound(max_possible_v+1, false);
                CompactBlockContainer listOfBlocks(nBlocks, n);
                initializeListOfBlocks(hash, rabinKarp, listOfBlocks,
                                        input, 0, n, level,
                                        plainMarks, buffer1);

                uint64_t segmentLength  = blockLengthLevel[level];
                uint64_t lim            = n - segmentLength;
                uint64_t i              = blockLengthLevel[level];

                //i'm currently using two input buffers, but we can simulate the
                //same by using just one. The problem appears if the given bufferReadSize
                //is smaller than the blockLengthLevel[level]
                IfstreamBuffer<T> inBufferHead(bufferReadSize, buffer1, 0);
                IfstreamBuffer<T> inBufferTail(bufferReadSize, buffer1, 0);
                assert(max_possible_v > 0);
                vector<ulong> counter(max_possible_v + 1, 0);
                for (size_t j = 0; j < i; j++) {
                    counter[inBufferHead[j]]++;
                    symbolFound[inBufferHead[j]] = true;
                }

                // for (uint j = 0; j <= max_possible_v; j++)
                //   ranksBlocks[j].push_back(0);//the first sample of counters is at position -1 --> they are all 0

                //compute the RK for the starting position
                // uint64_t v = rabinKarpBlock(inBufferHead, blockLengthLevel[level], blockLengthLevel[level] + segmentLength - 1, base, modulo);
                uint64_t fingerprint = rabinKarp.rkblock(inBufferHead, blockLengthLevel[level], blockLengthLevel[level] + segmentLength - 1);
                // vector<pair<ulong, ulong>> blockAndPointer;
                this->logger.INFO("Starting parsing", "BLockGraph");

                /*Iterate the input searching for the first occurrence of each block*/

                stringstream progress;
                do {
                    //compute the RK slide
                    fingerprint = rabinKarp.rkroll(fingerprint, inBufferTail[i], inBufferHead[i + segmentLength]);
#ifdef MYDEBUG
                    assert(input[i] == inBufferTail[i]);
#endif
                    ++i;
                    //assert(rabinKarpBlock(inBufferHead, i, i+segmentLength-1, base, modulo)==v);

                    auto got = hash.find(fingerprint);
                    if (got != hash.end()) {
                        //the signature for the current block is found in the hash
                        auto blockId = got->second;
                        uint64_t first_occ = listOfBlocks.getFirstOcc(blockId);
                        if (listOfBlocks.isUndefined(first_occ) ||
                            ((i % blockLengthLevel[level]) == 0ul &&
                             (blockId != i / blockLengthLevel[level]))) {
                            //check if actually the block covered by "block" is the same than that in [i,blockLengthLevel[level])
                            if (!listOfBlocks.isUndefined(first_occ)) {
                                assert((i % blockLengthLevel[level]) == 0);
                                //if we are covering exactly a block, we compute the hash and the returned value is
                                //not the block with id = i/blockLengthLevel[level], then it means that two blocks are equal and then
                                //they have the same signature. Thus, if block has a pointer to a previous block (it occurred before),
                                //then we have to set the pointer of the block i/blockLengthLevel[level] to where block points to.
                                //Note that each search in the hash for the same key always return the same block (since the hash is deterministic).
                                //Thus, anytime we search for v we obtain block, which is the one that has
                                //listOfBlocks contains a pointer to what it is stored in the Hash. Besides, the block "i" is not marked (not marked by default).
                                if (!plainMarks[i / blockLengthLevel[level]]) {
                                    // bpList.push_front(make_pair(i / blockLengthLevel[level], block->getFirstOcc()));
                                    // updateBlockAndPointer(i / blockLengthLevel[level], first_occ, blockAndPointer);
                                }
                                listOfBlocks.setFirstOcc(i / blockLengthLevel[level], first_occ);
#ifdef MYDEBUG
                                for (ulong j = 0; j < segmentLength; j++) {
                                    assert(input[i + j] == input[first_occ + j]);
                                }
#endif
                            } else {//however, if it is the first occurrence of a block (block->first_occ is set to -1)
                                //set the pointer for that block
                                listOfBlocks.setFirstOcc(blockId, i);
                                //mark the blocks that contains the first of occurrence of listOfBlocks[blockId]
                                uint64_t  iniBlock = i / blockLengthLevel[level],
                                          endBlock = iniBlock + ((i % blockLengthLevel[level]) ? 1 : 0);
                                plainMarks[i / blockLengthLevel[level]] = true;
                                plainMarks[iniBlock + ((i % blockLengthLevel[level]) ? 1 : 0)] = true;
                                //TODO: SEQ
                                // if (!plainMarks[blockId]) {
                                //     updateBlockAndPointer(blockId, i, blockAndPointer);
                                //     assert(plainMarks[blockId] == false);
                                // }
                            }
                        }
                    #ifdef HARDDEBUG
                        assert(SequenceLZBLockHelper::checkBlocks(input,(size_t) (listOfBlocks.getBlockId(blockId) * blockLengthLevel[level]), i,segmentLength ));
                    #endif
                    }
                } while (i < lim);
                this->logger.INFO(progress.str(), "BlockTree");
                this->logger.INFO("Second phase finished", "BlockTree");
                if (n % blockLengthLevel[level]) {
                    plainMarks[nBlocks - 1] = true;
                    listOfBlocks.setFirstOcc(nBlocks - 1, blockLengthLevel[level] * n / blockLengthLevel[level]);
                }
                inBufferHead.close();
                inBufferTail.close();

                //recompute max_v (now we accuretly now its value).
                min_v = max_possible_v;
                max_v = (T) 0;
                // auto min_max = minmax_element(symbolFound.begin(), symbolFound.begin()+max_possible_v);
                for (T i = 0; i < max_possible_v; i++){
                    if (symbolFound[i] < min_v) {
                        min_v = i;
                    }else if (symbolFound[i]>max_v){
                      max_v = i;
                    }
                  }

                PostProcessMarks    (level, plainMarks, nBlocks, listOfBlocks);
                CompressMarks       (level, plainMarks);
                pointer_new_input = storePointersBack(input, n, level, listOfBlocks, pointersUnmarked,
                                                      offsetsUnmarked, buffer1, buffer2);

#ifdef MYDEBUG
                assert(testSegmentsIndex(level, boundaries, pointer_new_input));
#endif
                this->logger.INFO("Building compressed BG","BlockTree");
                CompressBG(level, pointersUnmarked, offsetsUnmarked);
                //TODO: SEQUENCES RANK
                // this->logger.INFO("Building compressed rank","BlockTree");
                // buildCompressRank(level, ranksBlocks, ranksOffsets, symbolFound);

#ifdef MYDEBUG
                assert(testParsing(sequence_tests, level));
#endif
            }
          }
        return pointer_new_input;
    }

    template<typename T, class Leaves, class ExternalMod>void  BlockTree<T,Leaves,ExternalMod>::
      PostProcessMarks(uint level, vector<bool> &plainMarks, ulong nBlocks,
                     BlockContainer &listOfBlocks){
        uint64_t nMark = 0;
        for (uint64_t j = 0; j < nBlocks; j++) {
            if (listOfBlocks.isUndefined(listOfBlocks.getFirstOcc(j))) {
                listOfBlocks.setFirstOcc(j, j * blockLengthLevel[level]);
                plainMarks[j] = true;
            }
            if (plainMarks[j]) {
                nMark++;
            }
        }
        //if more than, say, 90% of the blocks of a level are marked, then I mark them all and
        //continue in the next level with smaller blocks (from n/r to n/r^2). I can do this if
        //the same was true for the previous levels (in that case firstLevel is equal to -1).
        if (firstLevel == -1 && nMark * 1.0 / plainMarks.size() >= 0.9) {
            for (uint64_t j = 0; j < nBlocks; j++) {
                if (!plainMarks[j]) {
                    plainMarks[j] = true;
                    listOfBlocks.setFirstOcc(j,j * blockLengthLevel[level]);
                }
            }
            // blockAndPointer.resize(0);//remove the elements of the backAndPointer (all of them are marked --> no pointers back)
        } else {
            //set this as the actual first level if it has not been set yet
            if (firstLevel == -1) {
                firstLevel = level;
            }
        }
    }

    template<typename T, class Leaves, class ExternalMod> ulong  BlockTree<T,Leaves,ExternalMod>::
      CompressMarks(uint level, vector<bool> &plainMarks){
        assert(bsb_marks);
        uint *bmp;
        createEmptyBitmap(&bmp, plainMarks.size());
        for (int64_t i = 0L; i < plainMarks.size(); i++) {
            if (plainMarks[i]) bit_set(bmp, i);
        }
        marks[level] = bsb_marks->build(bmp, plainMarks.size());
        assert(marks[level]->getLength()==plainMarks.size());

        delete[] bmp;
//#ifdef MYDEBUG
        {
            stringstream ss;
            ss << "Total marks level " << level << ": " << marks[level]->getLength() << ", marked: " <<
            marks[level]->rank1(marks[level]->getLength() - 1) << " (as % " <<
            marks[level]->rank1(marks[level]->getLength() - 1) * 100.0 / marks[level]->getLength() << ")" << endl;
            this->logger.INFO(ss.str(),"BlockTree");
        }
        //Marks level as firstLevel if the previous levels contain all blocks as marked but not in this.
        if (firstLevel==-1 && marks[level]->rank1(marks[level]->getLength()-1)!=marks[level]->getLength()){
            firstLevel = level;
        }
        return 0;
    }

    template<typename T, class Leaves, class ExternalMod>ulong  BlockTree<T,Leaves,ExternalMod>::
      CompressBG(uint level,vector<uint> &pointersUnmarked, vector<uint> &offsetsUnmarked){
        if (!pointersUnmarked.empty() && !offsetsUnmarked.empty()){
            transform(pointersUnmarked.begin(),pointersUnmarked.end(),pointersUnmarked.begin(),
                      [=](ulong x){return marks[level]->rank1(x)-1;});
            pointersBack[level] = new ArrayDA(&pointersUnmarked[0], pointersUnmarked.size());
            offsetsBack[level]  = new ArrayDA(&offsetsUnmarked[0],offsetsUnmarked.size());
        }
        return 0;
    }

    template <typename T, class Leaves, class ExternalMod> void  BlockTree<T,Leaves,ExternalMod>::
      initializeListOfBlocks(unordered_map<ulong, ulong> &hash,
                              const RabinKarp<T> &rk, BlockContainer &listOfBlocks,
                              T *input, const uint64_t start, const uint64_t end,
                              const int level, vector<bool> &plainMarks, string &nameBufferRead){
        uint64_t n = end-start;
        stringstream ss;
        //TODO: adjust the buffer sizes
        ss << "Function initialize list of blocks (first RK-pass)" << endl;
        ss << "Creating the input buffer" << endl;
        ss << "Buffer parameters: " << endl;
        ss << "\tbufferSize: " << bufferReadSize << endl;
        ss << "\tname: " << nameBufferRead << endl;

        IfstreamBuffer<T> it(bufferReadSize,nameBufferRead,0);

        ss << "Block length of " << blockLengthLevel[level] << " at level " << level << endl;
        uint64_t index = 0, block = 0, limSup = 0, delta = blockLengthLevel[level];
        //first block are directly marked
        ss << "Starting loop" << endl;
        ss << "n: " << n << endl;
        ss << "Increment: " << blockLengthLevel[level] << endl;
        ss << "BlockLengthLevel: " << blockLengthLevel[level] << endl;
        ss << "Start computing RK for each block" << endl;

        while ((limSup=index+blockLengthLevel[level])<=n){
            auto v = rk.rkblock(it, index, limSup-1);//(rabinKarpBlock(it,index,limSup-1,base, modulo);
            listOfBlocks.setUndefFirstOcc(block);
            hash[v] = block;
            index +=delta;
            block++;
        }
        listOfBlocks.setFirstOcc(0, 0);
        plainMarks[0]=true;
        ss << endl;
        ss << "Processing the remaining elements" << endl;
        //the last partial block...
        while (index<n) {
            auto v = rk.rkblock(it, index, (size_t)min((ulong)n - 1ul,(ulong)index+blockLengthLevel[level]));
            listOfBlocks.setUndefFirstOcc(block);
            hash[v] = block;
            index+=delta;
            block++;
        }
        ss << "First RK-pass finished" << endl;
        this->logger.INFO(ss.str(),"BlockTree");
    }

    template<typename T, class Leaves, class ExternalMod> void  BlockTree<T,Leaves,ExternalMod>::save(ofstream & fp) const
    {
        if (output==nullptr){
            for (int level=0;level<((int)this->strategySplitBlocks->getNLevels())-1;level++){
                saveLevel(fp, level,false);
            }
            bool saveLeaves = true;
            saveLevel(fp,this->strategySplitBlocks->getNLevels()-1,saveLeaves);
        }else{
//            this->logger.WARN("Warning: the sequence is already written on disk. Save function does nothing.","BlockTree");
            cout << "Warning: the sequence is already written on disk. Save function does nothing." << endl;
        }
    }

    /*
        This function can be called after the construction of each level or once the
        whole data structure is built. For the first case, saveLeaves must be set to false (by default
        is set to false). For the former, it must be when we are at the last level of the block
        graph (the level that contains the leaves). That is necessary to force the writting of the
        sequence of leaves.
     */
    //TODO: if all blocks are marked in a given level, the structures for rank are set to NULL, and thus we have problems
    //when trying to save/load them (as well as an space performance problem).
    template<typename T, class Leaves, class ExternalMod> void  BlockTree<T,Leaves, ExternalMod>::saveLevel(ofstream & fp, int level, bool saveLeaves) const
    {
        if (level==0) {
            uint wr = getHeader();
            saveValue(fp, wr);
            saveValue(fp, n);
            saveValue(fp, max_v);
            strategySplitBlocks->save(fp);

        }
        saveValue(fp,level);
        bool skipLevel = (level<firstLevel || firstLevel==-1);
        saveValue(fp,skipLevel);
        saveValue(fp, firstLevel);

        bool leaves = (seq_leaves!=nullptr && saveLeaves);
        saveValue(fp,leaves);
        if (skipLevel && !leaves) return;

        if (leaves) {
            saveValue(fp, blockLengthLevel[level]);
            saveValue(fp, degreeLevel[level]);
            seq_leaves->save(fp);
        }else {
            //intermediate level
            saveValue(fp, blockLengthLevel[level]);
            saveValue(fp, degreeLevel[level]);
            marks[level]->save(fp);
            pointersBack[level]->save(fp);
            offsetsBack[level]->save(fp);
            // saveLevelRank(fp, level);
        }
    }

    template<typename T, class Leaves, class ExternalMod> BlockTree<T,Leaves,ExternalMod>*  BlockTree<T,Leaves, ExternalMod>::load(ifstream & fp) {
        uint wr = loadValue<uint>(fp);
        switch(wr){
            // case SEQUENCE_BLOCK_GRAPH_HDR:
            //     return SequenceBlockTree<T,Leaves,ExternalMod>::load(fp);
            case BLOCKTREE_HDR:
                BlockTree<T,Leaves,ExternalMod> *ret = new BlockTree<T,Leaves,ExternalMod>();
                return ret->loadByLevels(fp);
        }
        return nullptr;
    }

    template<typename T, class Leaves, class ExternalMod> BlockTree<T,Leaves,ExternalMod> *  BlockTree<T,Leaves, ExternalMod>::loadByLevels(ifstream & fp) {

        seq_leaves = nullptr;
        output = nullptr;
        bool first = false;
        for(uint level=0;!seq_leaves;level++) {
            if (level == 0) {
                uint wr = loadValue<uint>(fp);
                if (wr!= getHeader()){
                    return nullptr;
                }
                n = loadValue<size_t>(fp);
                max_v = loadValue<T>(fp);
                strategySplitBlocks = StrategySplitBlock::load(fp);
            }

            uint lev = loadValue<uint>(fp);
            assert(lev==level);

            bool skipLevel = loadValue<bool>(fp);
            if (!skipLevel && !firstLevel){
                firstLevel = level;
            }

            firstLevel = loadValue<uint>(fp);

            //assert((skipLevel && (int)level<firstLevel) || !skipLevel);
            bool loadLeaves = loadValue<bool>(fp);
            if (loadLeaves) {
                auto bll = loadValue<ulong>(fp);
                blockLengthLevel.push_back(bll);
                auto d = loadValue<ulong>(fp);
                degreeLevel.push_back(d);
                seq_leaves = Sequence::load(fp);
            }else {
                //   size_t pos = fp.tellg();
                //intermediate level
                blockLengthLevel.push_back((skipLevel)?0:loadValue<ulong>(fp));
                degreeLevel.push_back((skipLevel)?0:loadValue<ulong>(fp));
                marks.push_back((skipLevel)?nullptr:BitSequence::load(fp));
                pointersBack.push_back((skipLevel)?nullptr:DirectAccess::load(fp));
                offsetsBack.push_back((skipLevel)?nullptr:DirectAccess::load(fp));

                if (!skipLevel) {
                    // loadByLevelsRank(fp, level);
                }
                //  size_t end_pos = fp.tellg();
                // assert((skipLevel && (pos==end_pos)) || (!skipLevel));
            }
        }
        return this;
    }


    template<typename T, class Leaves, class ExternalMod> void  BlockTree<T,Leaves,ExternalMod>::deleteLevel(uint level){

//        bool skipLevel = (firstLevel==-1) || ((int)level<firstLevel);
//        if (skipLevel) return;
        if (level>=this->firstLevel) {
            if (marks[level]) {
                delete marks[level];
                marks[level] = nullptr;
            }
            if (pointersBack[level]) {
                delete pointersBack[level];
                marks[level] = nullptr;
            }
            if (offsetsBack[level]) {
                delete offsetsBack[level];
                marks[level] = nullptr;
            }
            // deleteLevelRanks(level);
        }
    }

    template<typename T, class Leaves, class ExternalMod> ulong
      BlockTree<T,Leaves,ExternalMod>::storePointersBack(T *&input, uint64_t n, int level,
          BlockContainer &listOfBlocks,
          vector<uint> &pointersUnmarked,
          vector<uint> &offsetsUnmarked,
          string &bufferRead, string &bufferWrite){

#ifdef MYDEBUG
        vector<T> debInput(n);
        for (ulong j=0;j<n;j++)
            debInput[j]=input[j];

        T *new_input = new T[n];
        for (ulong i=0;i<n;i++)new_input[i]=input[i];
#endif

        //reserve space for the unmarked blocks
        uint64_t nBlocks    = marks[level]->getLength();
        uint64_t nNotMarked = marks[level]->rank0(marks[level]->getLength()-1);
        if (nNotMarked>0) {
            pointersUnmarked.reserve(nNotMarked);
            offsetsUnmarked.reserve(nNotMarked);
        }
        //we could move using rank/select. However, access is realy cheap using a PlainRepresenation
        //(a way more than select).

        uint64_t pointer_new_input = 0;
        IfstreamBuffer<T> it(bufferReadSize,bufferRead);
        OutBuffer <T> out(this->bufferWriteSize, bufferWrite);
        for (ulong i=0;i<nBlocks;i++){
            if (!marks[level]->access(i)) {
                auto pos = pointersUnmarked.size();
                pointersUnmarked.push_back(listOfBlocks.getFirstOcc(i) / blockLengthLevel[level]);
                offsetsUnmarked.push_back((pointersUnmarked[pos] + 1) * blockLengthLevel[level] - listOfBlocks.getFirstOcc(i) - 1);
            }else {
                auto ini = i * blockLengthLevel[level];
                auto end = min(n, static_cast<uint64_t>(ini + blockLengthLevel[level]));
                it[ini];
                for (ulong j = ini; j < end; j++) {
#ifdef MYDEBUG
                    new_input[pointer_new_input] = input[j];
                    positions[pointer_new_input] = positions[j];
#endif
                    pointer_new_input++;
                    out.push_back(*it);
                    it++;
                }
            }
        }
#ifdef MYDEBUG
        delete [] input;
        input = new_input;
#endif
        return pointer_new_input;
    }

    // template<typename T, class Leaves> void  BlockTree<T,Leaves,ExternalMod>::indexLeaves(string &inputBuffer,
    //                                                                            uint level, const vector<bool> &boundaries){
    //
    //     vector<uint> leaves;
    //     SequenceLZBLockHelper::loadAsUints<T>(inputBuffer,leaves);
    //     //The sequences implementations in libcds only take uint* as input.
    //     vector<uint> inputInts;
    //     inputInts.reserve(leaves.size());
    //     for (auto &x: leaves) inputInts.push_back((uint)x);
    //     seq_leaves = (Leaves*)new DAC(&inputInts[0],inputInts.size(),false);
    // }
    //
    // template<typename T, class Leaves> bool  BlockTree<T,Leaves,ExternalMod>::testParsing(vector<T> &sequence_test, uint level){
    //
    //     if ((int)level<firstLevel) return true;
    //     if ((uint)(level+1)==(uint)this->strategySplitBlocks->getNLevels()) return true;
    //     ulong segmentLength = blockLengthLevel[level]*(groupSize-1);
    //     auto nblocks = marks[level]->getLength();
    //     for (uint i=0;i<nblocks;i++){
    //         if (marks[level]->access(i)) continue;
    //         size_t block_index, block_pos, unmarked_index;
    //         getPointerBack(level,i, unmarked_index, block_index, block_pos);
    //
    //         auto block_end_pos = (block_pos+1)*blockLengthLevel[level];
    //         auto post_back_from_end = block_end_pos - (*offsetsBack[level])[unmarked_index]-1;
    //         for (size_t j=0;j<segmentLength;j++){
    //             assert(sequence_test[post_back_from_end+j]==sequence_test[i*blockLengthLevel[level]+j]);
    //         }
    //     }
    //     return true;
    // }



    /***********************************************************************************************/
    /***********************************************************************************************/
    /***********************************************************************************************/
    /***********************************************************************************************/

    // template<typename T, class Leaves> T  BlockTree<T,Leaves,ExternalMod>::accessLeaves(long pos) const{
    //     assert(pos<(long)seq_leaves->getLength());
    //     return (T)seq_leaves->access(pos);
    // }

    /**
    block: is the block that contains the position
    unmarked_index: how many unmarked blocks are before block
    block_index: marks->rank1(block)-1 (-1 because it is an index -indexes start in 0-)
    block_pos: index of the pointed block
    */
    template<typename T, class Leaves, class ExternalMod>uint  BlockTree<T,Leaves,ExternalMod>::getPointerBack(uint level, size_t block, size_t &unmarked_index, size_t &block_index, size_t &pointed_block_pos) const{
        assert(marks[level]->access(block)==0);
        unmarked_index      = marks[level]->rank0(block)-1;
        block_index         = (*pointersBack[level])[unmarked_index];
        pointed_block_pos   = marks[level]->select1(block_index+1);
        return 0;
    }
    template<typename T, class Leaves, class ExternalMod> T  BlockTree<T,Leaves,ExternalMod>::accessLeaves(long pos) const{
        assert(pos<(long)seq_leaves->getLength());
        return static_cast<T>(seq_leaves->access(pos));
    }
    template<typename T, class Leaves, class ExternalMod>uint  BlockTree<T,Leaves,ExternalMod>::operator[](size_t pos) const{
        uint level = firstLevel;
        if (level==-1)return accessLeaves(pos);
        while(!strategySplitBlocks->stop(level)){
            size_t block_index = pos / blockLengthLevel[level];
            if (!marks[level]->access(block_index)){
                //find the position "pos" corresponds to inside the block that is pointing
                size_t unmarked_index, block_index_of_1s,pblock_pos;
                getPointerBack(level,block_index, unmarked_index, block_index_of_1s, pblock_pos);
                size_t block_end_pos    = (pblock_pos+1)*blockLengthLevel[level];
                block_index             = pblock_pos;
                size_t block_ini        = block_end_pos - (*offsetsBack[level])[unmarked_index]-1;
                size_t offset           = pos % blockLengthLevel[level];
                pos                     = block_ini + offset;
            }
            //number of elements of marked blocks that are before block "block_index"
            pos = pos - marks[level]->rank0(block_index-1) * blockLengthLevel[level];
            level++;
        }
        return accessLeaves(pos);
    }
};

#endif
