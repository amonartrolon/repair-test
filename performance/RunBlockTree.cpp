//
// Created by Alberto Ordóñez Pereira on 14/06/2017.
//

//
// Created by Alberto Ordóñez Pereira on 30/05/2017.
//

#include <includes/experimental/BlockTree1.h>
#include <includes/experimental/CBlockTree.h>
#include <includes/utils/StringUtils.h>
#include <time.h>
#include <algorithm>


using namespace cds_utils;

string input_file = "/Users/aordonez/research/data/influenza.1MB.bin";

class OperationsHelper{
public:

    template<typename T> static void TestAccess(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                                         const cds_static::CBlockTree<T> &cbt1, uint64_t limit=0){
        uint64_t i = 0;
        if (limit == 0) --limit;
        for (; iter != end && i < limit; ++iter, i++){
            auto a = *iter;
            auto b = cbt1[i];
            assert(a == b);
        }
        std::cout << "Test access OK" << std::endl;
    }

    template<typename T>  static void TestRank(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                                        const cds_static::CBlockTree<T> &cbt1, uint64_t limit = 0){
        vector<uint32_t> rank(cbt1.sigma()+1);
        uint64_t i = 0;
        if (limit == 0) --limit;
        for (; iter != end && i < limit; ++iter, i++){
            auto a = ++rank[*iter];
            auto b = cbt1.rank(*iter, i);
            assert(a ==  b);
        }
        std::cout << "Test rank OK" << std::endl;
    }

    template<typename T> static void TestSelect(typename vector<T>::const_iterator iter, const typename vector<T>::const_iterator end,
                                          const cds_static::CBlockTree<T> &cbt1, uint64_t limit=0){
        vector<uint32_t> select(cbt1.sigma()+1);
        uint64_t i = 0;
        if (limit == 0) --limit;
        for (; iter != end && i < limit; ++iter, i++){
            ++select[*iter];
            auto b = cbt1.select(*iter, select[*iter]);
            assert(i ==  b);
        }
        std::cout << "Test select OK" << std::endl;
    }
};


int main(int argc, char** argv)
{
    if (argc < 3){
        std::cout << "Usage: " << argv[0] << " <input_file> block_length_level_0 [block_length_level_i...]" << std::endl;
        return 1;
    }
    string input_file = argv[1];
    vector<uint64_t> block_lengths;
    for (auto i = 2; i < argc; ++i){
        block_lengths.push_back(static_cast<uint64_t>(std::atoi(argv[i])));
    }
    vector<uint32_t> input;
    loadFile(input_file, input);
    for (uint64_t i=0; i < input.size() % block_lengths[0]; i++){
        input.push_back(0);
    }
    cds_static::CBlockTree<uint32_t> cbt1;
    auto sigma = static_cast<uint64_t>(*std::max_element(input.cbegin(), input.cend())+1);
    cds_static::StrategyBlockTree strategy(block_lengths.cbegin(), block_lengths.cend());

    auto begin = std::chrono::high_resolution_clock::now();

    cbt1.build(input.cbegin(), input.cend(), input.size(), sigma, std::move(strategy));

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << cbt1.stats() << std::endl;
    std::cout << cbt1.size_bytes() * 8.0 / cbt1.size() << std::endl;
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() * 1.0 / cbt1.size() << "ns/symbol" << std::endl;

    OperationsHelper::TestAccess<uint32_t>(input.cbegin(), input.cend(), cbt1, 1000000);
    OperationsHelper::TestRank<uint32_t>(input.cbegin(), input.cend(), cbt1, 1000000);
    OperationsHelper::TestSelect<uint32_t>(input.cbegin(), input.cend(), cbt1, 1000000);
    return 0;
}
