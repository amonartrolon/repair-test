//
// Created by Alberto Ordóñez Pereira on 1/5/16.
//


#include <cstdlib>
#include <sequence/wt_coder_huff.h>
#include <sequence/wt_coder_SubOptimalMult.h>
#include <sequence/wt_coder_hutucker.h>
#include <utils/timeMeasure.h>
#include <utils/BitStreamIterator.h>

#ifndef OUTPUT_FILE
#define OUTPUT_FILE "tmp.bin"
#endif

using namespace std;
using namespace cds_static;
using namespace cds_utils;

const uint timeFactor = 1000000000;
uint x = 10;
uint b = 10;
void usage(char *name){
    cerr << "Usage: " << name<< " collection (t[n,s,e]|p|a|m|h|i|o) [options (s| l (t|p)| l (t|p) c )]" << endl;
    cerr << "\tt: table" << endl;
    cerr << "\tp: perm" << endl;
    cerr << "\ta: add" << endl;
    cerr << "\tm: mult" << endl;
    cerr << "\th: hu-tucker" << endl;
    cerr << "\ti: pMin" << endl;
    cerr << "\tb: balanced" << endl;
    cerr << "\to: optimal l-restricted" << endl;
    cerr << "OPTIONS: " << endl;
    cerr << "\ts: sampling for select improvement on permutation"<<endl;
    cerr << "\tl: lmax (for add and mult)" << endl;
    cerr << "\tc: for mult or minFreq for pMin" << endl;
}

void run_code_decode(char **argv, uint *input, const uint len, ifstream &inputf, uint REP, MappingSymbols *ms,
                     double &time_code, double &time_decode){
    //MEASURING TIME
    double t0,t1;
    //CODING
    uint *bmp_out;
    createEmptyBitmap(&bmp_out,len*W);
    string stream_of_bits("tmp.stream.bits.bin");
    BitStreamOutputWriter *bsow = new BitStreamOutputWriter(stream_of_bits);
    inputf.open(argv[1]);
    t0=getTime();
    for (uint j=0;j<REP;j++){
        for (uint i=0;i<len;i++){
            uint l, code, leaf,symb;
            inputf.read((char*)&symb, sizeof(uint));
            ms->getCode(symb,code,l,leaf);
            bsow->write(invertWord(code)>>(W-l),l);
        }
    }
    delete bsow;
    t1 = getTime();
    time_code = t1-t0;

    inputf.close();

    //DECODING
    BitStreamIteratorDisk bsid(stream_of_bits);
    uint current_buffer = 0;
    bsid.readAndShift(32,current_buffer);
    t0=getTime();
    for (uint j=0;j<REP;j++){
        ofstream out_symbols(OUTPUT_FILE);
        vector<uint> symbols(32);
        uint n_symbols=0;
        for (uint i=0;i<len;i+=n_symbols){
            n_symbols = 0;
            uint l  = ms->getSymbols(current_buffer,n_symbols,symbols);
            assert(n_symbols>0);
            bsid.readAndShift(l,current_buffer);
            out_symbols.write((char*)&symbols[0],sizeof(uint)*n_symbols);
            assert(symbols[0]==input[i]);
        }
    }
    t1 = getTime();
    time_decode = t1-t0;
}

void run_code_decode(char **argv, uint *input, const uint len, ifstream &inputf, uint REP, wt_coder *coder,
                     double &time_code, double &time_decode){
    //MEASURING TIME
    double t0,t1;
    //CODING
    uint *bmp_out;
    createEmptyBitmap(&bmp_out,len*W);
    string stream_of_bits("tmp.stream.bits.bin");
    BitStreamOutputWriter *bsow = new BitStreamOutputWriter(stream_of_bits);
    inputf.open(argv[1]);
    t0=getTime();
    for (uint j=0;j<REP;j++){
        for (uint i=0;i<len;i++){
            uint l,code,symb;
            inputf.read((char*)&symb, sizeof(uint));
            l = coder->getCode(symb,code);
            bsow->write(invertWord(code)>>(W-l),l);
        }
    }
    delete bsow;
    t1 = getTime();
    time_code = t1-t0;

    inputf.close();

    //DECODING
    BitStreamIteratorDisk bsid(stream_of_bits);
    uint current_buffer = 0;
    bsid.readAndShift(32,current_buffer);
    t0=getTime();
    for (uint j=0;j<REP;j++){
        ofstream out_symbols(OUTPUT_FILE);
        vector<uint> symbols(32);
        uint n_symbols=0;
        for (uint i=0;i<len;i+=n_symbols){
            n_symbols = 0;
            uint l  = coder->getSymbols(current_buffer,n_symbols,symbols);
            assert(n_symbols>0);
            bsid.readAndShift(l,current_buffer);
            out_symbols.write((char*)&symbols[0],sizeof(uint)*n_symbols);
            assert(symbols[0]==input[i]);
        }
    }
    t1 = getTime();
    time_decode = t1-t0;
}

void compute_optimality(char **argv, const uint len, const uint max_v, pair<uint,uint> *codeLen, MappingSymbols *ms,
                        double &pi_li_optimal, double &optimallity){
    //LOADING FREQS FILE
    char *bufferFreqs  = new char[strlen(argv[1])+128];
    uint *freqs;

    strcpy(bufferFreqs,argv[1]);
    strcat(bufferFreqs,".freqs");
    ifstream in(bufferFreqs);
    in.read((char*)&len,sizeof(uint));
    in.read((char*)&max_v,sizeof(uint));
    freqs = new uint[max_v+1];
    in.read((char*)freqs,sizeof(uint)*(max_v+1));
    in.close();

    pi_li_optimal = 0.0;
    for (uint i=0;i<=max_v;i++){
        double pi = freqs[i]*1.0/len;
        pi_li_optimal += pi * log(pi)/log(2);
    }

    optimallity=0.0;
    for (uint i=0;i<=max_v;i++){
        uint leaf;
        ms->getCode(i,codeLen[i].second,codeLen[i].first,leaf);
        double pi = static_cast<double>(freqs[i])/len;
        if (pi<=0){
            cerr << endl;
        }
        //assert(pi>0);
        optimallity += freqs[i]*1.0/len*codeLen[i].first;
    }
    optimallity = -optimallity;

    delete [] bufferFreqs;
    delete [] freqs;

}

void compute_optimality(char **argv, const uint len, const uint max_v, pair<uint,uint> *codeLen, wt_coder *ms,
                        double &pi_li_optimal, double &optimallity){
    //LOADING FREQS FILE
    char *bufferFreqs  = new char[strlen(argv[1])+128];
    uint *freqs;

    strcpy(bufferFreqs,argv[1]);
    strcat(bufferFreqs,".freqs");
    ifstream in(bufferFreqs);
    in.read((char*)&len,sizeof(uint));
    in.read((char*)&max_v,sizeof(uint));
    freqs = new uint[max_v+1];
    in.read((char*)freqs,sizeof(uint)*(max_v+1));
    in.close();

    pi_li_optimal = 0.0;
    for (uint i=0;i<=max_v;i++){
        double pi = freqs[i]*1.0/len;
        pi_li_optimal += pi * log(pi)/log(2);
    }

    optimallity=0.0;
    for (uint i=0;i<=max_v;i++){
        codeLen[i].second = ms->getCode(i,codeLen[i].first);
        double pi = static_cast<double>(freqs[i])/len;
        assert(pi>0);
        optimallity += freqs[i]*1.0/len*codeLen[i].second;
    }
    optimallity = -optimallity;

    delete [] bufferFreqs;
    delete [] freqs;

}

void compute_h0(uint *input, uint len){
    uint max_v=0;
    for (uint i=0;i<len;i++)
        max_v = max(max_v,input[i]);
    vector<uint> counter(max_v+1,0);
    for (uint i=0;i<len;i++)
        counter[input[i]]++;
    double h0=0.0;
    for (uint i=0;i<=max_v;i++){
        if (counter[i]==0) {

            cerr << "Input not continuous" << endl;
            abort();
        }
        double pi = static_cast<double>(counter[i])/len;
        h0+=pi*log(1.0/pi)/log(2);
    }
    cerr << "h0: " << h0 << endl;
}

bool lexSmaller(uint a, uint b, uint lena, uint lenb){
    uint min_len = min(lena,lenb);
    for (uint i=1;i<min_len;i++){
        uint ba,bb;
        ba =((a>>(lena-i))&0x1u);
        bb = ((b>>(lenb-i))&0x1u);
        if (ba!=bb){
            if ( ba > bb){
                cerr << "a: " << a << ", b: " << b << endl;
                cerr << ((a>>(lena-i))&0x1u) << "," <<((b>>(lenb-i))&0x1u) << endl;
                return false;
            }
            return true;
        }

    }
    return true;
}

bool prefixFree(cds_static::tuple *symbols, uint nsymb){
    for (uint i=0;i<nsymb-1;i++){
        for (uint j=i+1;j<nsymb;j++){
            if (symbols[i].len < symbols[j].len){
                if (symbols[i].code == (symbols[j].code >> (symbols[j].len-symbols[i].len))){
                    cerr << "Not perfix free" << endl;
                    abort();
                }
            }
        }
    }
    return true;
}

void run_hutucker(char **argv, uint *input){
    char *bufferFreqs  = new char[strlen(argv[1])+128];
    uint *freqs;
    uint len, max_v;
    strcpy(bufferFreqs,argv[1]);
    strcat(bufferFreqs,".freqs");
    ifstream in(bufferFreqs);
    in.read((char*)&len,sizeof(uint));
    in.read((char*)&max_v,sizeof(uint));
    freqs = new uint[max_v+1];
    in.read((char*)freqs,sizeof(uint)*(max_v+1));
    in.close();
    strcpy(bufferFreqs,argv[1]);
    strcat(bufferFreqs,".hutucker");
    wt_coder *coder;
    cds_static::tuple *table_symbols_codes = nullptr;
    ifstream ht_file(bufferFreqs);
    if (ht_file.good()){
        uint sigma_ht = loadValue<uint>(ht_file);
        uint len_ht = loadValue<uint>(ht_file);
        uint *par = loadValue<uint>(ht_file,uint_len(len_ht,1));
        table_symbols_codes = new cds_static::tuple[sigma_ht];
        table_symbols_codes = loadValue<cds_static::tuple>(ht_file,sigma_ht);

        ht_file.close();

    }else{
        ht_file.close();
        coder = new wt_coder_hutucker(freqs,max_v+1);
        ofstream out(bufferFreqs);
        coder->save(out);
        out.close();
        cerr << "Rerun the program to obtain code/decode times and space performance" << endl;
        exit(0);
    }
    prefixFree(table_symbols_codes,max_v+1);

    for (uint i=0;i<max_v;i++){
        if (!lexSmaller(table_symbols_codes[i].code,table_symbols_codes[i+1].code,table_symbols_codes[i].len,table_symbols_codes[i+1].len)) {
            cerr << "First: " << table_symbols_codes[i].code << "(len: " << table_symbols_codes[i].len << "), second: " << table_symbols_codes[i+1].code << " (len: " << table_symbols_codes[i+1].len << ")" << endl;
            abort();
        }

    }
    uint costRank = 50;
    uint costFindClose = 250;
    size_t totalCostCompr = 0;
    size_t totalCostDecompr = 0;
    uint *unos = new uint[max_v+1];
    for (uint i=0;i<=max_v;i++){
        unos[i]=0;
        for (uint j=0;j<table_symbols_codes[i].len;j++){
            if (bitget(&(table_symbols_codes[i].code),j)){
                unos[i]++;
            }
        }
    }

    float optimallity = 0.0;
    for (uint i=0;i<=max_v;i++){

        optimallity += freqs[i]*1.0/len*table_symbols_codes[i].len;
    }
    cerr << "Optimality ht: " << optimallity;
    for (uint i=0;i<len;i++){
        totalCostDecompr = totalCostDecompr + (2*costRank + costFindClose)* unos[input[i]];
        totalCostCompr = totalCostCompr + (2*costRank * table_symbols_codes[input[i]].len) + costFindClose*unos[input[i]];
    }
    cerr << "Time(decomp): " << totalCostDecompr * 1.0 / len << endl;
    cerr << "Time(comp): " << totalCostCompr * 1.0 / len << endl;
    delete coder;
    delete [] freqs;

//    vector<pair<uint,uint>> lengths_codes;
//    uint costRank = 50;
//    uint costFindClose = 250;
//    size_t totalCostCompr = 0;
//    size_t totalCostDecompr = 0;
//    uint *unos = new uint[max_v+1];
//    for (uint i=0;i<=max_v;i++){
//        unos[i]=0;
//        pair<uint,uint>len_code;
//        len_code.first = coder->getCode(i,len_code.second);
//        lengths_codes.push_back(len_code);
//        for (uint j=0;j<len_code.first;j++){
//            if (bitget(&(len_code.second),j)){
//                unos[i]++;
//            }
//        }
//    }
//
//    float optimallity = 0.0;
//    for (uint i=0;i<=max_v;i++){
//
//        optimallity += freqs[i]*1.0/len*lengths_codes[i].first;
//    }
//    cerr << "Optimality ht: " << optimallity;
//    for (uint i=0;i<len;i++){
//        totalCostDecompr = totalCostDecompr + (2*costRank + costFindClose)* unos[input[i]];
//        totalCostCompr = totalCostCompr + (2*costRank * lengths_codes[input[i]].first ) + costFindClose*unos[input[i]];
//    }
//    cerr << "Time(decomp): " << totalCostDecompr * 1.0 / len << endl;
//    cerr << "Time(comp): " << totalCostCompr * 1.0 / len << endl;
//    delete coder;
//    delete [] freqs;
}
int main(int argc, char ** argv) {

    if (argc<3){
        usage(argv[0]);
        exit(0);
    }

    int samplingSelect = 0;
    uint len=0,max_v=0;
    auto file_length = [](const string &input){
        ifstream in(input);
        in.seekg(0,ios_base::end);
        auto ret = in.tellg();
        in.close();
        return ret/sizeof(uint);
    };
    len = static_cast<uint>(file_length(string(argv[1])));

    uint *input = new uint[len];
    uint *inputCodes = new uint[len];
    ifstream inputf(argv[1]);
    inputf.read((char*)input,sizeof(uint)*len);
    inputf.close();
    max_v = 0;
    for (ulong i=0;i<len;i++) max_v = max(max_v,input[i]);
    pair<uint,uint> *codeLen = new pair<uint,uint>[max_v+1];
    compute_h0(input,len);
    wt_coder_huff * wc;
    char *buffer  = new char[strlen(argv[1])+128];
    strcpy(buffer,argv[1]);
    strcat(buffer,".huffman.model");
    ifstream in(buffer);
    wc = wt_coder_huff::load(in);//wt_coder_huff * wc = new wt_coder_huff(input, len, new MapperNone());
    in.close();
    delete [] buffer;
    assert(max_v==wc->getMaxv());


    uint REP = 0;
    char option = argv[2][0];
    if (option == 'h'){
        run_hutucker(argv,input);
    }else {
        if (option == 't' || option == 'p') {
            if (option == 'p' && argc < 4) {
                usage(argv[0]);
                exit(0);
            }
            //MODEL STORED IN A TALBLE (t) or PERMUTATION (p)

            wt_coder_huff_can *can = new wt_coder_huff_can(wc, max_v);
            cds_static::tuple *table_symbols_codes = can->getCopyTableSymbols();

            uint maxCode = 0;
            uint minCode = static_cast<uint>(~0);
            float avgCode = 0.0;
            for (uint i = 0; i <= max_v; i++) {
                avgCode += table_symbols_codes[i].len;
                if (table_symbols_codes[i].len > maxCode) maxCode = table_symbols_codes[i].len;
                if (table_symbols_codes[i].len < minCode) minCode = table_symbols_codes[i].len;
            }
            avgCode = avgCode / (max_v + 1);

            MappingSymbols *ms;
            if (argv[2][0] == 't') {
                if (argv[2][1] == 'n') {
                    ms = new MappingTableNoAcelerator(table_symbols_codes, (size_t) max_v + 1);
                } else if (argv[2][1] == 's') {

                    ms = new MappingTableAceleartorStart(table_symbols_codes, (size_t) max_v + 1, b);
                } else if (argv[2][1] == 'e') {

                    ms = new MappingTableAceleratorExtended(table_symbols_codes, (size_t) max_v + 1, x);
                } else {
                    usage(argv[0]);
                    return 0;
                }
            } else {
                BitSequenceBuilder *bsb;
                samplingSelect = atoi(argv[3]);

                if (samplingSelect < 0) {
                    BitSequencePlain::BuilderSequencePlain builder = BitSequencePlain::BuilderSequencePlain::createBuilderSequencePlain(nullptr, 0, 33)[0];//->s1factor(samplingSelect).s0factor(samplingSelect);
                    bsb = new BitSequenceBuilderPlain(&builder);
                } else {
                    BitSequencePlain::BuilderSequencePlain builder = BitSequencePlain::BuilderSequencePlain::createBuilderSequencePlain(nullptr, 0, 33)->s1factor(samplingSelect).s0factor(samplingSelect);
                    bsb = new BitSequenceBuilderPlain(&builder);
                }
                ms = new MappingPermutations(table_symbols_codes, (size_t) max_v + 1, bsb);
            }

            double time_code, time_decode, optimallity, pi_li_optimal;
            compute_optimality(argv, len, max_v, codeLen, ms, pi_li_optimal, optimallity);
            run_code_decode(argv, input, len, inputf, REP, ms, time_code, time_decode);
            cout << "Collection: " << argv[1] << ";samplingSelect: " << samplingSelect;
            cout << ";Option " << argv[2] << ";sigma: " << max_v << ";minle: " << minCode;
            cout << ";maxle: " << maxCode << ";avgCo: " << avgCode << ";timeCode: " << time_code * timeFactor / ((len * REP));
            cout << ";size: " << ms->getSize() * 8.0 / (max_v + 1) << ";optimallity: " << optimallity;
            cout << ";timeDeco: " << time_decode * timeFactor / ((len * REP)) << endl;
            cout << "-->size: " << ms->getSize() * 8.0 / (max_v + 1) << ";timeCode: " << time_code * timeFactor / ((len * REP));
            cout << ";size: " << ms->getSize() * 8.0 / (max_v + 1) << ";timeDeco: " << time_decode * timeFactor / ((len * REP)) << endl;

        } else {
            uint lmax;
            float c;
            //CODER
            wt_coder *coder;

            if (option == 'a' || option == 'm' || option == 'i' || option == 'b' || option == 'o') {
                //ADDITIVE HUFFMAN MODEL (a) or MULTIPLICATIVE (m)
                if (option == 'a' && argc < 5) {
                    usage(argv[0]);
                    exit(0);
                }
                if ((option == 'm' || option == 'i') && argc < 7) {
                    usage(argv[0]);
                    exit(0);
                }

                samplingSelect = atoi(argv[3]);
                BitSequenceBuilder *bsb;
                if (samplingSelect < 0) {
                    BitSequencePlain::BuilderSequencePlain builder = BitSequencePlain::BuilderSequencePlain::createBuilderSequencePlain(nullptr, 0, 33)[0];//->s1factor(samplingSelect).s0factor(samplingSelect);
                    bsb = new BitSequenceBuilderPlain(&builder);
                } else {
                    BitSequencePlain::BuilderSequencePlain builder = BitSequencePlain::BuilderSequencePlain::createBuilderSequencePlain(nullptr, 0, 33)->s1factor(samplingSelect).s0factor(samplingSelect);
                    bsb = new BitSequenceBuilderPlain(&builder);
                }

                //Decide if Permutation or Tabl
                MappingBuilder *mb = nullptr;
                if (option == 'a' || option == 'b' || option == 'o') {

                    if (argv[5][0] == 'p') {
                        mb = new MappingPermutationsBuilder(bsb);
                    } else {
                        if (argv[5][1] == 'n') {
                            mb = new MappingTableBuilder();
                        } else if (argv[5][1] == 's') {

                            mb = new MappingTableBuilderStart(b);
                        } else if (argv[5][1] == 'e') {

                            mb = new MappingTableBuilderExtended(x);
                        } else {
                            usage(argv[0]);
                            exit(0);
                        }
                    }
                } else if (option == 'm' || option == 'i') {
                    cerr << argv[6] << endl;
                    if (argv[6][0] == 'p') {
                        mb = new MappingPermutationsBuilder(bsb);
                    } else {
                        if (argv[6][1] == 'n') {
                            mb = new MappingTableBuilder();
                        } else if (argv[6][1] == 's') {

                            mb = new MappingTableBuilderStart(b);
                        } else if (argv[6][1] == 'e') {

                            mb = new MappingTableBuilderExtended(x);
                        } else {
                            usage(argv[0]);
                            exit(0);
                        }
                    }
                }

                max_v = wc->getMaxv() - 1;
                lmax = static_cast<uint>(atoi(argv[4]));

                //Compute the approximated model
                if (option == 'a') {
                    coder = new wt_coder_SubOptimalAdd(wc, wc->getMaxv(), lmax, mb);
                } else if (option == 'm') {
                    REP=1;
                    c = static_cast<float>(atof(argv[5]));
                    //a multiplicative coder is built on an additive one
                    wt_coder_SubOptimalAdd *wc_add = new wt_coder_SubOptimalAdd(wc, wc->getMaxv(), lmax, mb);
                    coder = new wt_coder_SubOptimalMult(wc_add, wc->getMaxv(), c, lmax);
                } else if (option == 'i') {
                    c = static_cast<float>(atof(argv[5]));
                    coder = new wt_coder_Pmin(input, len, lmax, static_cast<uint>(c), mb);
                } else if (option == 'b') {
                    coder = new wt_coder_Balanced(wc, wc->getMaxv(), lmax, mb);
                } else if (option == 'o') {
                    //cerr << "suboptimal" << endl;
                    coder = new wt_coder_Suboptimal(input, len, lmax, mb);
                } else {
                    cerr << "option " << option << " not known" << endl;
                    return 0;
                }

            } else {
                usage(argv[0]);
                exit(0);
            }

            double optimallity, pi_li_optimal, time_code, time_decode;
            compute_optimality(argv, len, max_v, codeLen, coder, pi_li_optimal, optimallity);
            run_code_decode(argv, input, len, inputf, REP, coder, time_code, time_decode);

            cout << "Collection: " << argv[1] << ";samplingSelect: " << samplingSelect;
            cout << ";Option " << argv[2];
            if (option == 'a' || option == 'i' || option == 'b' || option == 'o') {
                cout << ";lmax:" << lmax;
            } else if (option == 'm') {
                cout << ";lmax:" << lmax << ";c:" << c;
            }
            coder->avgCodeLength();
            cout << ";timeCode: " << time_code * timeFactor / ((len * REP));
            cout << ";size: " << coder->getSize() * 8.0 / (max_v + 1);
            cout << ";optimallity: " << optimallity;
            cout << ";timeDeco: " << time_decode * timeFactor / ((len * REP)) << endl;
            cout << "-->size: " << coder->getSize() * 8.0 / (max_v + 1) << ";timeCode: " << time_code * timeFactor / ((len * REP));
            cout << ";size: " << coder->getSize() * 8.0 / (max_v + 1) << ";timeDeco: " << time_decode * timeFactor / ((len * REP)) << endl;
        }
    }
    delete [] input;
    delete [] inputCodes;
    delete [] codeLen;
    return 0;

}
